/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tomp2p.audiovideowrapper.audiovideo;

import java.nio.ByteBuffer;

import java.io.Closeable;
import java.io.IOException;

/**
 *
 * @author draft
 */
public interface AudioData extends Closeable {

    public void created(ByteBuffer buffer) throws IOException;
    
}
