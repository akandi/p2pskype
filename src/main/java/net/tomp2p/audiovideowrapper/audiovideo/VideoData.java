/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tomp2p.audiovideowrapper.audiovideo;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 *
 * @author draft
 */
public interface VideoData extends Closeable {

    public void created(ByteBuffer buffer, long time, int w, int h) throws IOException;
    
}
