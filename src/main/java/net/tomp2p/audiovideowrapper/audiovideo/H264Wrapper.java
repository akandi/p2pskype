/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tomp2p.audiovideowrapper.audiovideo;

import com.github.sarxos.webcam.Webcam;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicInteger;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import net.tomp2p.audiovideowrapper.fileio.FrameSeparatorInputStream;
import net.tomp2p.audiovideowrapper.fileio.FrameSeparatorStream;
import net.tomp2p.audiovideowrapper.net.VideoReceiver;
import net.tomp2p.audiovideowrapper.net.VideoSender;

import javax.imageio.ImageIO;
import javax.sound.sampled.LineUnavailableException;
import org.jcodec.codecs.h264.H264Decoder;
import org.jcodec.codecs.h264.H264Encoder;
import org.jcodec.common.model.ColorSpace;
import org.jcodec.common.model.Picture;
import org.jcodec.scale.AWTUtil;
import org.jcodec.scale.RgbToYuv420j;
import org.jcodec.scale.Transform;
import org.jcodec.scale.Yuv420jToRgb;

/**
 *
 * @author draft
 */
public class H264Wrapper {

  private final static int FPS = 10;

  private static int w = 0, h = 0;
  private static boolean running = true;
  
  private static VideoReceiver channel;

  public static int getW() {
    return w;
  }

  public static int getH() {
    return h;
  }
  
  public static VideoData send(InetAddress address, int port) throws IOException {
    VideoSender channel = new VideoSender(address, port);

    return new VideoData() {
      @Override
      public void created(ByteBuffer bb, long time, int w0, int h0) throws IOException {
        w = w0;
        h = h0;
        if (bb.limit() == 0) {
          return;
        }
       
        bb.position(0);
        channel.send(bb, time, w, h);
      }

      @Override
      public void close() throws IOException {
        channel.close();
      }
    };
  }
  
  public static VideoData decodeAndStoreToFile(String filename) throws IOException {
    final Transform transform2 = new Yuv420jToRgb();
    final H264Decoder decoder = new H264Decoder(); // qp
    
    FileOutputStream fos = new FileOutputStream(Paths.get("./" + filename).toFile());
    FrameSeparatorStream fss = new FrameSeparatorStream(fos);

    return new VideoData() {
      private Picture target1 = null;
      private Picture rgb = null;
      private BufferedImage bi = null;

      @Override
      public void created(ByteBuffer bb, long time, int w0, int h0) {
        try {
          w = w0;
          h = h0;
          if (bb.limit() == 0) {
            return;
          }
          if (target1 == null && rgb == null && bi == null) {
            target1 = Picture.create((w + 15) & ~0xf, (h + 15) & ~0xf, ColorSpace.YUV420J);
            rgb = Picture.create(w, h, ColorSpace.RGB);
            bi = new BufferedImage(w, h, BufferedImage.TYPE_3BYTE_BGR);
          }

          Picture dec = decoder.decodeFrame(bb, target1.getData());
          transform2.transform(dec, rgb);
          AWTUtil.toBufferedImage(rgb, bi);
          
          ImageIO.write(bi, "jpg", fss);
          fss.markFrame();
          
        } catch (Exception e) {
          e.printStackTrace();
        }
      }

      @Override
      public void close() throws IOException {
        fss.close();
        fos.close();
      }
    };
  }

  public static VideoData decodeAndPlay(final ImageView imageView) throws LineUnavailableException {
    final Transform transform2 = new Yuv420jToRgb();
    final H264Decoder decoder = new H264Decoder(); // qp

    return new VideoData() {
      private long baseline = 0;
      private Picture target1 = null;
      private Picture rgb = null;
      private BufferedImage bi = null;

      @Override
      public void created(ByteBuffer bb, long time, int w0, int h0) {
        try {
          w = w0;
          h = h0;
          if (bb.limit() == 0) {
            return;
          }
          if (target1 == null && rgb == null && bi == null) {
            target1 = Picture.create((w + 15) & ~0xf, (h + 15) & ~0xf, ColorSpace.YUV420J);
            rgb = Picture.create(w, h, ColorSpace.RGB);
            bi = new BufferedImage(w, h, BufferedImage.TYPE_3BYTE_BGR);
          }

          Picture dec = decoder.decodeFrame(bb, target1.getData());
          transform2.transform(dec, rgb);
          AWTUtil.toBufferedImage(rgb, bi);
          WritableImage newImg = SwingFXUtils.toFXImage(bi, null);

          // FPS stuff
          long now = System.currentTimeMillis();
          if (baseline == 0) {
            baseline = now - time;
          } else {
            long wait = (baseline + time) - now;
            if (wait > 0) {
              Thread.sleep(wait);
            } /*
               * else { System.out.println("FPS too high in decoding!" + wait);
               * }
               */
          }
          imageView.setImage(newImg);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }

      @Override
      public void close() throws IOException {
        // nothing ?
      }
    };
  }

  public static void stopRecordAndEncode() {
    running = false;
    channel.close();
  }
  
  public static void readFromSocket(final VideoData onFrame, int port) throws IOException {
    channel = new VideoReceiver(port, new VideoReceiver.FrameReceivedHandler() {
      
      @Override
      public void handle(ByteBuffer buf, long time, int w, int h) {
        try {
          if (running) {
            buf.limit(buf.position());
            buf.position(0);
            
            onFrame.created(buf, time, w, h);
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }
  
  public static void readFromFileAndEncode(final VideoData onFrame, String filename) throws IOException {
    final H264Encoder encoder = new H264Encoder(); // qp
    final Transform transform = new RgbToYuv420j();
    final AtomicInteger counter = new AtomicInteger(0);

    FileInputStream fis = new FileInputStream(Paths.get("./" + filename).toFile());
    FrameSeparatorInputStream fsis = new FrameSeparatorInputStream(fis);
    
    new Thread(new Runnable() {
      int frameCount = 0;
      
      @Override
      public void run() {
        try {
          long start, wait;
          Picture yuv = null;
                  
          while (running) {
            start = System.currentTimeMillis();
            BufferedImage rgb = ImageIO.read(fsis);
            fsis.skipToNextFrame();
            
            if (rgb != null) {
              if (yuv == null) {
                w = rgb.getWidth();
                h = rgb.getHeight();
                yuv = Picture.create(w, h, ColorSpace.YUV420);
              }
              transform.transform(AWTUtil.fromBufferedImage(rgb), yuv);
              final ByteBuffer ff = encoder.encodeFrame(yuv, ByteBuffer.allocate(w * h * 3));
  
              if (counter.get() == 0) {
                Platform.runLater(new Runnable() {
                  final int frameCnt = frameCount;
                  
                  @Override
                  public void run() {
                    try {
                      counter.incrementAndGet();
                      onFrame.created(ff, frameCnt * 100, w, h);
                      counter.decrementAndGet();
                    } catch (Exception e) {
                      e.printStackTrace();
                    }
                  }
                });
                
                frameCount++;
              }
            }

            // FPS stuff
            wait = (start + (1000 / FPS)) - System.currentTimeMillis();
            if (wait > 0) {
              Thread.sleep(wait);
            } /*
               * else { System.out.println("FPS too high in encoding!" + wait);
               * }
               */
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }).start();
  }

  public static void recordAndEncode(final Webcam webcam0, final VideoData onFrame) {
    Webcam webcam = webcam0;
    webcam.open();
    final H264Encoder encoder = new H264Encoder(); // qp
    final Transform transform = new RgbToYuv420j();
    final AtomicInteger counter = new AtomicInteger(0);

    running = true;
    
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          long start, wait;
          Picture yuv = null;
          while (running) {
            start = System.currentTimeMillis();
            BufferedImage rgb = webcam.getImage();
            if (yuv == null) {
              w = rgb.getWidth();
              h = rgb.getHeight();
              yuv = Picture.create(w, h, ColorSpace.YUV420);
            }
            transform.transform(AWTUtil.fromBufferedImage(rgb), yuv);
            final ByteBuffer ff = encoder.encodeFrame(yuv, ByteBuffer.allocate(w * h * 3));

            if (counter.get() == 0) {
              Platform.runLater(new Runnable() {
                @Override
                public void run() {
                  try {
                    counter.incrementAndGet();
                    onFrame.created(ff, System.currentTimeMillis(), w, h);
                    counter.decrementAndGet();
                  } catch (Exception e) {
                    e.printStackTrace();
                  }
                }
              });
            }

            // FPS stuff
            wait = (start + (1000 / FPS)) - System.currentTimeMillis();
            if (wait > 0) {
              Thread.sleep(wait);
            } /*
               * else { System.out.println("FPS too high in encoding!" + wait);
               * }
               */
          }
          
          webcam.close();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }).start();
  }
}
