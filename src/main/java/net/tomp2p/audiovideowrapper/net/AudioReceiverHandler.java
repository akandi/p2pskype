package net.tomp2p.audiovideowrapper.net;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;

import net.tomp2p.audiovideowrapper.net.AudioReceiver.FrameReceivedHandler;

public class AudioReceiverHandler extends SimpleChannelUpstreamHandler {
  Channel channel;  
  FrameReceivedHandler handler;
  
  public AudioReceiverHandler(FrameReceivedHandler handler) {
    super();
    this.handler = handler;
  }

  @Override
  public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
    ChannelBuffer buffer = (ChannelBuffer)e.getMessage();
    byte[] data = new byte[buffer.readableBytes()];
    buffer.readBytes(data);
    
    handler.handle(data);
    
    super.messageReceived(ctx, e);
  }
}
