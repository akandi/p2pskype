package net.tomp2p.audiovideowrapper.net;

import org.jboss.netty.bootstrap.ConnectionlessBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.InternetProtocolFamily;
import org.jboss.netty.channel.socket.nio.NioDatagramChannelFactory;

import java.net.InetSocketAddress;

/**
 * Discards any incoming data.
 */
public final class AudioReceiver {
  Channel channel;
  ConnectionlessBootstrap bootstrap;
  
  public AudioReceiver(int port, FrameReceivedHandler handler) {
    // Configure the bootstrap.
    bootstrap = new ConnectionlessBootstrap(
        new NioDatagramChannelFactory(InternetProtocolFamily.IPv4));

    bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
      public ChannelPipeline getPipeline() {
        ChannelPipeline p = Channels.pipeline();

        p.addLast("streaming", new AudioReceiverHandler(handler));
        
        return p;
      }
    });

    channel = bootstrap.bind(new InetSocketAddress(port));
  }

  public void close() {
    try {
      channel.close();
      bootstrap.releaseExternalResources();
    } catch (Exception e) {
      // nothing
    }
  }
  
  public interface FrameReceivedHandler {
    public void handle(byte[] data);
  }
}
