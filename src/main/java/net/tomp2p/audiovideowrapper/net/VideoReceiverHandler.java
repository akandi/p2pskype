package net.tomp2p.audiovideowrapper.net;

import java.nio.ByteBuffer;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;

import net.tomp2p.audiovideowrapper.net.VideoReceiver.FrameReceivedHandler;
import net.tomp2p.audiovideowrapper.net.VideoReceiver.Packet;

/**
 * Handles a server-side channel.
 */
public class VideoReceiverHandler extends SimpleChannelUpstreamHandler {
  FrameReceivedHandler handler;
  long lastTimestamp = -1;
  ByteBuffer buf = ByteBuffer.allocate(50000); // should hopefully accomodate all packets
  
  public VideoReceiverHandler(FrameReceivedHandler handler) {
    super();
    this.handler = handler;
  }

  @Override
  public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
    if (e.getMessage() instanceof Packet) {
      Packet packet = (Packet)e.getMessage();
      
      if (packet.time == lastTimestamp || lastTimestamp == -1) {
        buf.put(packet.data);
        lastTimestamp = packet.time;
      } else if (packet.time > lastTimestamp) {
        handler.handle(buf, lastTimestamp, packet.w, packet.h);
        buf.clear();
        
        buf.put(packet.data);
        lastTimestamp = packet.time;
      } else {} // we have a packet out of order, but are already processing the next frame -> discard
    }
    
    super.messageReceived(ctx, e);
  }
}
