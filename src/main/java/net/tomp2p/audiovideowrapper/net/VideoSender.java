package net.tomp2p.audiovideowrapper.net;

import org.jboss.netty.bootstrap.ConnectionlessBootstrap;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.InternetProtocolFamily;
import org.jboss.netty.channel.socket.nio.NioDatagramChannelFactory;
import org.jboss.netty.handler.codec.serialization.ObjectEncoder;

import challengetask.application.util.Constants;
import net.tomp2p.audiovideowrapper.net.VideoReceiver.Packet;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

/**
 * Discards any incoming data.
 */
public final class VideoSender {  
  ChannelFuture channelFuture;
  ConnectionlessBootstrap bootstrap;
  VideoSenderHandler sender = new VideoSenderHandler();
  
  public VideoSender(InetAddress address, int port) {
    // Configure the bootstrap.
    bootstrap = new ConnectionlessBootstrap(
        new NioDatagramChannelFactory(InternetProtocolFamily.IPv4));

    bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
      public ChannelPipeline getPipeline() {
        ChannelPipeline p = Channels.pipeline();

        p.addLast("serialization", new ObjectEncoder(Constants.VIDEO_PACKET_PAYLOAD_SIZE + 500));
        p.addLast("streaming", sender);
        
        return p;
      }
    });

    try {
      channelFuture = bootstrap.connect(new InetSocketAddress(address, port)).sync();
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
  
  public void send(ByteBuffer buf, long time, int w, int h) throws IOException {
    int numPackets = (int)Math.ceil((double)buf.limit() / Constants.VIDEO_PACKET_PAYLOAD_SIZE);
    
    int bufferLimit = buf.limit();
    
    for (int i = 0; i < numPackets; i++) {
      if(!sender.channelIsOpen()) throw new IOException("Channel not open");

      int step = i * Constants.VIDEO_PACKET_PAYLOAD_SIZE;
      int size = Constants.VIDEO_PACKET_PAYLOAD_SIZE;
      
      if (Constants.VIDEO_PACKET_PAYLOAD_SIZE > bufferLimit - step) size = bufferLimit - step;
      
      byte[] data = new byte[size];
      buf.get(data);
      sender.send(new Packet(data, time, w, h));
    }
  }
  
  public void close() {
    try {
      channelFuture.getChannel().close();
      bootstrap.releaseExternalResources();
    } catch (Exception e) {
      // nothing
    }
  }
}
