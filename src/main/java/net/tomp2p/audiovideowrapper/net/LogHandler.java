package net.tomp2p.audiovideowrapper.net;

import org.jboss.netty.channel.ChannelEvent;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;

public class LogHandler extends SimpleChannelUpstreamHandler {
  @Override
  public void handleUpstream(ChannelHandlerContext ctx, ChannelEvent e) throws Exception {
    super.handleUpstream(ctx, e);
    
    log(ctx, e);
  }
  
  @Override
  public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
    super.messageReceived(ctx, e);
    
    log(ctx, e);
  }
  
  private void log(ChannelHandlerContext ctx, ChannelEvent e) {
    System.out.println(ctx);
    System.out.println(e);
  }
}
