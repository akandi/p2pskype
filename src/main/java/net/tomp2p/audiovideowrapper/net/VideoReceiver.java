package net.tomp2p.audiovideowrapper.net;

import org.jboss.netty.bootstrap.ConnectionlessBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.InternetProtocolFamily;
import org.jboss.netty.channel.socket.nio.NioDatagramChannelFactory;
import org.jboss.netty.handler.codec.serialization.ClassResolvers;
import org.jboss.netty.handler.codec.serialization.ObjectDecoder;

import java.io.Serializable;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

public final class VideoReceiver {
  Channel channel;
  ConnectionlessBootstrap bootstrap;
  
  public VideoReceiver(int port, FrameReceivedHandler handler) {
    // Configure the bootstrap.
    bootstrap = new ConnectionlessBootstrap(
        new NioDatagramChannelFactory(InternetProtocolFamily.IPv4));

    bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
      @Override
      public ChannelPipeline getPipeline() {
        ChannelPipeline p = Channels.pipeline();

        p.addLast("deserialization", new ObjectDecoder(ClassResolvers.cacheDisabled(null)));
        //p.addLast("logging", new LogHandler());
        p.addLast("streaming", new VideoReceiverHandler(handler));
        
        return p;
      }
    });

    channel = bootstrap.bind(new InetSocketAddress(port));
  }
  
  public interface FrameReceivedHandler {
    public void handle(ByteBuffer data, long time, int w, int h);    
  }
  
  public static class Packet implements Serializable {
    private static final long serialVersionUID = 3934352802638472304L;

    public byte[] data;
    public long time;
    public int w;
    public int h;

    public Packet(byte[] data, long time, int w, int h) {
      this.data = data;
      this.time = time;
      this.w = w;
      this.h = h;
    }
  }

  public void close() {
    try {
      channel.close();
      bootstrap.releaseExternalResources();
    } catch (Exception e) {
      // nothing
    }
  }
}
