package net.tomp2p.audiovideowrapper.net;

import net.tomp2p.audiovideowrapper.net.VideoReceiver.Packet;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;

import challengetask.application.util.Constants;

import java.io.IOException;

public class VideoSenderHandler extends SimpleChannelUpstreamHandler {  
  Channel channel;  
  
  @Override
  public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
    channel = e.getChannel();
    
    super.channelConnected(ctx, e);
  }
  
  @Override
  public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
    channel = null;
    
    super.channelDisconnected(ctx, e);
  }

  public void send(Packet packet) throws IOException {
    // if (channel == null) throw new IOException("Channel not open");
    try {
      channel.write(packet).sync();
      Thread.sleep(Constants.VIDEO_SLEEP_AFTER_PACKET);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public boolean channelIsOpen(){
    return this.channel != null;
  }
}
