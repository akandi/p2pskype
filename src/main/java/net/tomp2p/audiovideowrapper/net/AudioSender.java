package net.tomp2p.audiovideowrapper.net;

import org.jboss.netty.bootstrap.ConnectionlessBootstrap;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.InternetProtocolFamily;
import org.jboss.netty.channel.socket.nio.NioDatagramChannelFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;

/**
 * Discards any incoming data.
 */
public final class AudioSender {
  AudioSenderHandler sender = new AudioSenderHandler();
  ChannelFuture channelFuture;
  ConnectionlessBootstrap bootstrap;
  
  public AudioSender(InetAddress address, int port) {
    // Configure the bootstrap.
    bootstrap = new ConnectionlessBootstrap(
        new NioDatagramChannelFactory(InternetProtocolFamily.IPv4));

    bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
      public ChannelPipeline getPipeline() {
        ChannelPipeline p = Channels.pipeline();
        
        p.addLast("streaming", sender);
        
        return p;
      }
    });

    channelFuture = bootstrap.connect(new InetSocketAddress(address, port));

  }
  
  public void send(byte[] data) throws IOException {
    sender.send(data);
  }

  public void close() {
    try {
      channelFuture.getChannel().close();
      bootstrap.releaseExternalResources();
    } catch (Exception e) {
      // nothing
    }
  }
}
