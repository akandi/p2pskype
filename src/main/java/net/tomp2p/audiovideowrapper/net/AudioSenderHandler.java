package net.tomp2p.audiovideowrapper.net;

import java.io.IOException;

import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;

public class AudioSenderHandler extends SimpleChannelUpstreamHandler {
  Channel channel;  
  
  @Override
  public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
    channel = e.getChannel();
    
    super.channelConnected(ctx, e);
  }
  
  @Override
  public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
    channel = null;
    
    super.channelDisconnected(ctx, e);
  }

  public void send(byte[] data) throws IOException {
    // if (channel == null) throw new IOException("Channel not open");
    
    try {
      channel.write(ChannelBuffers.wrappedBuffer(data));
    } catch (Exception e) {
      // channel already closed
    }
  }
}
