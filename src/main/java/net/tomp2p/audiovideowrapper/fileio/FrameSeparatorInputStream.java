package net.tomp2p.audiovideowrapper.fileio;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FrameSeparatorInputStream extends InputStream {
  FileInputStream stream;
  FileChannel channel;
  long currentLength;
  long currentPos;
  
  public FrameSeparatorInputStream(FileInputStream fis) throws IOException {
    stream = fis;
    channel = fis.getChannel();
    readLength();
    currentPos = channel.position();
  }
  
  private void readLength() throws IOException {
    ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
    int read = channel.read(buffer);
    if (read == -1) {
      currentPos = -1;
    } else if (read != 8) {
      throw new IOException("unexpected behavior");
    }
    buffer.position(0);
    currentLength = buffer.getLong();
  }

  @Override
  public int read() throws IOException {
    if (currentPos == -1) return -1;
    return stream.read();
  }
  
  @Override
  public int read(byte[] b, int off, int len) throws IOException {
    if (currentPos == -1) return -1;
    return stream.read(b, off, len);
  }
  
  public void skipToNextFrame() throws IOException {
    if (currentPos == -1) return;
    
    currentPos += 8 + currentLength;
    channel.position(currentPos - 8);
    readLength();
    try {
      Thread.sleep(1);
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
}
