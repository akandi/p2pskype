package net.tomp2p.audiovideowrapper.fileio;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FrameSeparatorStream extends OutputStream {
  FileOutputStream stream;
  FileChannel channel;
  long lengthPos;
  
  public FrameSeparatorStream(FileOutputStream os) throws IOException {
    stream = os;
    channel = os.getChannel();
    
    lengthPos = channel.position();
    channel.position(lengthPos + 8);
  }

  @Override
  public void write(int b) throws IOException {
    stream.write(b);
  }
  
  @Override
  public void write(byte[] b, int off, int len) throws IOException {
    stream.write(b, off, len);
  }
  
  public void markFrame() throws IOException {
    stream.flush();
    long length = channel.position() - lengthPos - 8;
    long pos = channel.position();
    channel.position(lengthPos);
    ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
    buffer.putLong(length);
    buffer.position(0);
    channel.write(buffer, lengthPos);
    lengthPos = pos;
    channel.position(pos + 8);
  }
}
