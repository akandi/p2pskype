package gui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import challengetask.application.interfaces.User;
import gui.model.Friend;
import gui.model.FriendRequestIncoming;
import gui.model.FriendRequestOutgoing;
import gui.model.Person;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.util.Callback;

public class FriendList {
  
  private Map<String, Person> friends = new HashMap<String, Person>();
  private ObservableList<Person> target;

  public void setFriendList(ObservableList<Person> target) {
    this.target = target;
    this.target.clear();
    friends.clear();
  }
  
  public void addUnconfirmed(User friend) {
    Person friendRequest = new FriendRequestOutgoing(friend.getName());
    friends.put(friend.getId(), friendRequest);
    target.add(friendRequest);
  }

  public void addIncoming(User friend, Callback<String, Void> clickHandler) {
    Person friendRequest = new FriendRequestIncoming(friend.getName());
    friendRequest.addClickHandler(clickHandler);
    friends.put(friend.getId(), friendRequest);
    target.add(friendRequest);
  }

  public void update(List<User> newFriends) {
    for (User friend : newFriends) {
      Person person = friends.get(friend.getId());
      
      /* cases:
       *   friend request outgoing rejected: only disappears after application restart (would have to regularly check network or be notified about changes) 
       *   friend request outgoing accepted: will be removed and friend added instead
       *   friend request incoming rejected: handled in removeIncoming
       *   friend request incoming accepted: will be removed and friend added instead
       *   loading friend list from the network on application start: else case every time creates friends
       */
      if (person != null) {
        if (person instanceof FriendRequestIncoming || person instanceof FriendRequestOutgoing) {
          target.remove(person);
          Person newPerson = new Friend(friend.getName());
          friends.put(friend.getId(), newPerson);
          target.add(newPerson);
        }
        person.nameProperty().set(friend.getName());
      } else {
        Person newFriend = new Friend(friend.getName());
        friends.put(friend.getId(), newFriend);
        target.add(newFriend);
      }
    }
  }

  public void removeIncoming(User user) {
    Person person = friends.get(user.getId());
    target.remove(person);
    friends.remove(user.getId());
  }
  
  public void updateChatOut(String receiverId,String senderName, SortedMap<Long, String> newMessages){
	  Person person = friends.get(receiverId);
	  if (person != null){
		  person.addChatOut(senderName, newMessages);
	  }
	  updateAllChat(senderName);
  }
  
  public void updateChatIn(String receiverId,String senderName, SortedMap<Long, String> newMessages){
	  Person person = friends.get(receiverId);
	  if (person != null){
		  person.addChatIn(newMessages);
	  }
	  updateAllChat(senderName);
  }
  
  public String getIdByPerson(Person person){
	  for (Map.Entry<String,Person> e : friends.entrySet()){
		  if (e.getValue().equals(person))
			  return e.getKey();
	  }
	  return null;
  }
  
  public void updateAllChat(String senderName){
	  for (Map.Entry<String,Person>  e:friends.entrySet()){
		  e.getValue().updateEntireChat(senderName);
	  }
  }

  public void updateOnlineStatus(String personId, boolean online) {
	  System.out.println("updating online"+personId+" "+online);
    Person friend = friends.get(personId);
    if (friend != null) {
  	  System.out.println("updating online2"+personId+" "+online);
  	Platform.runLater(new Runnable() {
		
			@Override
			public void run() {
				System.out.println("update runlater"+personId+online);

      friend.setOnline(online);
			}
  	});
    }
  }
}
