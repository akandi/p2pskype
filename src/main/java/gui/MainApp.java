package gui;

import challengetask.application.P2PSkypeImpl;
import challengetask.application.interfaces.*;
import challengetask.application.util.NotLoggedInException;
import challengetask.application.util.UserNotFoundException;
import gui.model.Person;
import gui.view.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.SortedMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class MainApp extends Application {

    private Stage stage;
    private Person person;
    private P2PSkype p2pSkype = new P2PSkypeImpl();
    private FriendList friendList = new FriendList();
    
    private VideoController videoWindow = null;

    // this is used for incoming, as well as outgoing calls because once they are set up they are the same
    private VideoCallCallback videoCallCallback = null;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;
        stage.setTitle("Login");
        stage.getIcons().add(new Image("file:resources/images/skype_32.png"));
        goToLogin();
        stage.show();
    }
    @Override
    public void stop() {
    	getP2pSkype().shutdown();
    	
    	
    }
    
    public Person getPerson() {
        return person;
    }

    public P2PSkype getP2pSkype() {
        return p2pSkype;
    }

    public boolean userLogin(String name){
        
        if (p2pSkype.tryLogin(name)) {
            person = new Person("---");
            friendList.setFriendList(person.getFriends());
            
            p2pSkype.subscribeFriendsListChanged(new FriendsListChangedCallback() {
                @Override
                public void call(List<User> friends) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {

                    friendList.update(friends);
                        }
                    });
                }
            });
            
            p2pSkype.subscribeFriendRequest(new FriendRequestCallback() {
                private boolean resolved = false;
                private boolean result = false;
                private final Object lock = new Object();
              
                @Override
                public boolean displayRequest(User user) {
                	resolved=false;
                	result=false;
                    final FriendRequestCallback cb = this;
                    friendList.addIncoming(user, new Callback<String, Void>() {
                        @Override
                        public Void call(String name) {
                            Platform.runLater(new Runnable() {
                                @Override public void run() {
                                    cb.requestResolved(handleIncomingFriendRequest(name));
                                }});
                            return null;
                        }
                    });
                    
                    while(!resolved) {
                        synchronized (lock) {
                            try {
                                lock.wait();
                            } catch (InterruptedException e) {                              
                                e.printStackTrace();
                            }
                        }                    
                    }
                    
                    // remove incoming request, will be replaced by real friend item if result == true
                    friendList.removeIncoming(user);
                    
                    return result;
                }
                
                @Override
                public void requestResolved(boolean decision) {
                    result = decision;
                    resolved = true;     
                    synchronized (lock) {
                        lock.notify();
                    }
                }
            });
            
            p2pSkype.subscribeConversationChanged(new ConversationChangedCallback() {
    			
          			@Override
          			public void callOut(String receiverId, SortedMap<Long, String> newMessages) {
          				  javafx.application.Platform.runLater(() -> friendList.updateChatOut(receiverId,person.getName(),newMessages));
          			}

        				@Override
        				public void callIn(String receiverId, SortedMap<Long, String> newMessages) {
            				javafx.application.Platform.runLater(() -> friendList.updateChatIn(receiverId,person.getName(),newMessages));
        				}
            });
            
            p2pSkype.subscribeOnlineStatusChanged(new OnlineStatusChangedCallback() {              
                @Override
                public void update(String personId, boolean online) {
		                    friendList.updateOnlineStatus(personId, online);
							

                }
            });
            
            p2pSkype.subscribeIncomingVideoCall(new IncomingVideoCallCallback() {
                private final Object lock = new Object();
                private Boolean result = null;
              
                @Override
                public void handleIncomingCall(VideoCallCallback cb) {   
                    result = null;
                    setVideoCallCallback(cb);
                    
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Incoming video connection");
                            alert.setHeaderText("");
                            alert.setContentText("Do you want to accept the video connection?");
                           
                            result = alert.showAndWait().get() == ButtonType.OK;
                            
                            synchronized (lock) {
                                lock.notify();
                            }
                        }                     
                    });  
                    
                    while (result == null) {
                        synchronized (lock) {
                            try {
                                lock.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    
                    cb.setAccept(result);
                }
            });
            
            goToFriendsList();
            return true;
        } else {
            return false;
        }
    }
    
    private ImageView openVideoWindow() {
        ImageView result = null;
        if (videoWindow == null) {
            try {
                URL path = getClass().getClassLoader().getResource("fxml/Video.fxml");
                Stage stage = new Stage();
                videoWindow = (VideoController) newStage(path, "Video Stream", stage);
                videoWindow.setApp(this);
                videoWindow.setStage(stage);
                result = videoWindow.getVideoView();
                
            } catch (Exception e) {
                e.printStackTrace();
            }   
        } else {
          result = videoWindow.getVideoView();
          videoWindow.show();
        }
        
        videoCallCallback.onCloseWindow(new Callback<Void, Void>() {
          public Void call(Void param) {
              Platform.runLater(new Runnable() {
                  @Override
                  public void run() {
                    videoWindow.close();  
                  }
              });
              
              return null;
          }
      });
        
        return result;
    }

    public void terminateVideo(){
        videoCallCallback.terminateSelfAndOther();
    }

    private void goToLogin() {
        try {
            URL path = getClass().getClassLoader().getResource("fxml/Login.fxml");
            LoginController login = (LoginController) replaceSceneContent(path);
            login.setApp(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void goToFriendsList(){
        try {
            URL path = getClass().getClassLoader().getResource("fxml/FriendsList.fxml");
            FriendsListController friends = (FriendsListController) replaceSceneContent(path);
            friends.setApp(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Initializable replaceSceneContent(URL fxml) throws Exception{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(fxml);
        AnchorPane page = null;
        try{
            page = (AnchorPane) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(page);
        stage.setScene(scene);
        return (Initializable) loader.getController();
    }

    private Initializable newStage(URL fxml, String title){
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(fxml);
        AnchorPane page = null;
        try{
            page = (AnchorPane) loader.load();
        } catch (IOException e){
            e.printStackTrace();
        }
        Scene scene = new Scene(page);
        Stage stage = new Stage();
        stage.setTitle(title);
        stage.setScene(scene);
        stage.show();
        return (Initializable) loader.getController();
    }
    
    private Initializable newStage(URL fxml, String title, Stage stage){
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(fxml);
      AnchorPane page = null;
      try{
          page = (AnchorPane) loader.load();
      } catch (IOException e){
          e.printStackTrace();
      }
      Scene scene = new Scene(page);
      stage.setTitle(title);
      stage.setScene(scene);
      stage.show();
      return (Initializable) loader.getController();
  }

    public void listPeers() {
        try {
            URL path = getClass().getClassLoader().getResource("fxml/PeerList.fxml");
            PeerListController peerList = (PeerListController) newStage(path, "Peer List");
            peerList.setApp(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void showRequestLogfile() {
        try {
            URL path = getClass().getClassLoader().getResource("fxml/RequestLogfile.fxml");
            RequestLogfileController peerList = (RequestLogfileController) newStage(path, "Request Logfile");
            peerList.setApp(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static class FileDialogFuture implements Future<File> {
        final Object lock = new Object();
        File result = null;
      
        @Override
        public boolean cancel(boolean mayInterruptIfRunning) { return false; }
  
        @Override
        public boolean isCancelled() { return false; }
  
        @Override
        public boolean isDone() {
            synchronized (lock) {
                return result != null;
            }
        }
  
        @Override
        public File get() throws InterruptedException, ExecutionException {
            synchronized (lock) {
                lock.wait();
                return result;
            }
        }
  
        @Override
        public File get(long timeout, TimeUnit unit)
                throws InterruptedException, ExecutionException, TimeoutException {
            throw new ExecutionException(new Exception("not implemented"));
        }          
    }
    
    public void requestLogfile(gui.model.Peer peer) {
        FileDialogFuture fileDialogFuture = new FileDialogFuture();
            
        // have to remove leading slash from ip address
        String ip = peer.getIp().get();
        ip = ip.substring(1, ip.length());
        p2pSkype.sendLogfileRequest(ip, peer.getPort().get(), fileDialogFuture);
            
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Logfile to");
        File result = fileChooser.showSaveDialog(stage); 
            
        synchronized (fileDialogFuture.lock) {
            fileDialogFuture.result = result;
            fileDialogFuture.lock.notify();
        }
    }

    public void addFriend() {
        TextInputDialog dialog = new TextInputDialog("walter");
        
        dialog.setTitle("Send Friend Request");
        dialog.setHeaderText("Send Friend Request");
        dialog.setContentText("Send request to:");
  
        Optional<String> result = dialog.showAndWait();
  
        result.ifPresent(id -> {
            try {
                User friend = p2pSkype.sendFriendRequest(id);
                
                friendList.addUnconfirmed(friend);
            } catch (NotLoggedInException | UserNotFoundException | IOException e) {
                e.printStackTrace();
            }
        });
    }

    public boolean handleIncomingFriendRequest(String name) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Accept Friend Request");
        alert.setHeaderText("Accept Friend Request");
        alert.setContentText("Do you want to accept " + name + " as a friend?");
  
        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }
    public void sendChatMessage(Person receiver,String message){
    	p2pSkype.sendChatMessage(friendList.getIdByPerson(receiver),message);
    }
   public void setPersonName(String newName){
	   person.SetName(newName);
   }
    public void updateAllChat(){
    	javafx.application.Platform.runLater(() ->friendList.updateAllChat(person.getName()));
    }
    
    private void setVideoCallCallback(VideoCallCallback cb) {
        videoCallCallback = cb;
        
        videoCallCallback.onRequireVideoWindow(new VideoWindowCallback() {
          private final Object lock = new Object();
          private ImageView res = null;
          
          @Override
          public ImageView createWindow() {
              Platform.runLater(new Runnable() {
                  @Override
                  public void run() {
                      res = openVideoWindow();
                      synchronized (lock) {
                          lock.notify();
                      }
                  }
              });
                          
              synchronized (lock) {
                  try {
                    lock.wait();
                  } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                  }
              }
              
              return res;              
          }
      });
    }

    public void sendVideoRequest(Person receiver){
        String id = friendList.getIdByPerson(receiver);
        
        VideoCallCallback cb = p2pSkype.sendVideoRequest(id);
        setVideoCallCallback(cb);
        
        cb.setOnDenied(new Callback<Void, Void>() {
            @Override
            public Void call(Void param) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("Video Call Denied");
                        alert.setHeaderText("Video Call Denied");
                        alert.setContentText("The video call didn't succeed");
                        
                        alert.show();
                    }
                });
                
                return null;
            }
        });
    }
}
