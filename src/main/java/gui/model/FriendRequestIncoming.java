package gui.model;

public class FriendRequestIncoming extends Person {

  public FriendRequestIncoming(String name) {
    super(name);
    this.name.set(rawName + " (requests friendship)");
  }
}
