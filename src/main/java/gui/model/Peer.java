package gui.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Peer {
  private StringProperty ip;
  private StringProperty port;
  private StringProperty version;
  
  // TODO maybe also store and display the number160 peer address and use it for the logfile request
  
  public Peer(String ip, String port, String version) {
    this.ip = new SimpleStringProperty(ip);
    this.port = new SimpleStringProperty(port);
    this.version = new SimpleStringProperty(version);
  }
  
  public StringProperty getIp() {
    return ip;
  }

  public StringProperty getPort() {
    return port;
  }

  public StringProperty getVersion() {
    return version;
  }
}
