package gui.model;

import javafx.beans.InvalidationListener;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

/**
 * Created by Marc on 20.04.16.
 */
public class Person {
    private static ImageView onlineImg = null;
    private static ImageView offlineImg = null;
  
    protected String rawName;
    protected StringProperty name;
    private BooleanProperty online;
    private ObservableList<Person> friends;
    private ObservableMap<Long, String> messages;
    private ObservableList<String> chat;
    private SortedMap<Long,String> messagesMap=new TreeMap<Long,String>();
    private SortedMap<Long,String> messagesMapIn=new TreeMap<Long,String>();
    private ImagePropertyWrapper imagePropertyWrapper;
    
    private Callback<String, Void> clickHandler;

    
    SimpleDateFormat newFormat = new SimpleDateFormat("HH:mm:ss");    

    
    public Person(String name){
        try {
            onlineImg = new ImageView(new Image(getClass().getClassLoader().getResource("css/icons/online.png").toString()));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        try {
            offlineImg = new ImageView(new Image(getClass().getClassLoader().getResource("css/icons/offline.png").toString()));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
      
        this.rawName = name;
        this.name = new SimpleStringProperty(name);
        this.online = new SimpleBooleanProperty(true);
        this.friends = FXCollections.observableArrayList();
        this.chat=FXCollections.observableArrayList();
        this.imagePropertyWrapper=new ImagePropertyWrapper();
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty(){
        return name;
    }

    public boolean getOnline() {
        return online.get();
    }
    
    private final class ImagePropertyWrapper implements ObservableValue<ImageView> {
      Map<ChangeListener<? super ImageView>, ChangeListener<Boolean>> listeners = new HashMap<>();
      
      @Override
      public void addListener(InvalidationListener listener) {
          Person.this.online.addListener(listener);
      }

      @Override
      public void removeListener(InvalidationListener listener) {
          Person.this.online.removeListener(listener);
      }

      @Override
      public void addListener(ChangeListener<? super ImageView> listener) {
          ChangeListener<Boolean> theListener = new ChangeListener<Boolean>() {
              @Override
              public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
                      Boolean newValue) {
                  listener.changed(ImagePropertyWrapper.this, getValue(oldValue), getValue(newValue));
              }                
          };
          
          listeners.put(listener, theListener);
        
          Person.this.online.addListener(theListener);
      }

      @Override
      public void removeListener(ChangeListener<? super ImageView> listener) {
          Person.this.online.removeListener(listeners.get(listener));
      }
      
      private ImageView getValue(boolean value) {
          System.out.println("wrapper"+ Person.this.getName()+value);

        return value ? onlineImg : offlineImg;
      }

      @Override
      public ImageView getValue() {
          return getValue(Person.this.getOnline());
      }          
  }

    public ObservableValue<ImageView> onlineProperty() {
        // wrap the boolean property in an image view property
        // TODO maybe this can be done better
      System.out.println("wrapper"+ Person.this.getName());
        return imagePropertyWrapper;
    }
    public BooleanProperty onlinePropertytext(){
    	return online;
    	
    }

    public ObservableList<Person> getFriends() {
        return friends;
    }
    
    public void addClickHandler(Callback<String, Void> cb) {
        this.clickHandler = cb;
    }
    
    public void click() {
        clickHandler.call(rawName);
    }
    public void addChatIn(SortedMap<Long, String> newMessages){
    	Long lastShown;
    	if (!messagesMapIn.isEmpty() )
    	  lastShown = messagesMapIn.lastKey();
    	else{
    		chat.clear();
    		lastShown=0L;
    	}
    	if (newMessages.lastKey() > lastShown){
    		SortedMap<Long, String>  newM =newMessages.subMap(lastShown + 1, newMessages.lastKey() + 1);
            for (Entry<Long, String> message : newM.entrySet()) {
            	messagesMapIn.put(message.getKey(),message.getValue());
    		}
    	}
        this.messages = FXCollections.observableMap(messagesMap);
    }
    public void addChatOut(String senderName, SortedMap<Long, String> newMessages){
    	Long lastShown;
    	if (!messagesMap.isEmpty() )
    	  lastShown = messagesMap.lastKey();
    	else{
    		chat.clear();
    		lastShown=0L;
    	}
    	if (newMessages.lastKey() > lastShown){
    		SortedMap<Long, String>  newM =newMessages.subMap(lastShown+1, newMessages.lastKey()+1);
            for (Entry<Long, String> message : newM.entrySet()) {
            	messagesMap.put(message.getKey(),message.getValue());
    		}
    	}
        this.messages=FXCollections.observableMap(messagesMap);
    }
    public void updateEntireChat(String senderName){
    	SortedMap<Long, Entry<Integer,String>> sortedMessages=new TreeMap<Long, Entry<Integer,String>>();
    	for (Entry<Long, String> message : messagesMap.entrySet())
    		sortedMessages.put(message.getKey(), new  AbstractMap.SimpleEntry<Integer,String>(1,message.getValue()));
    	for (Entry<Long, String> message : messagesMapIn.entrySet())
    		sortedMessages.put(message.getKey(), new  AbstractMap.SimpleEntry<Integer,String>(0,message.getValue()));
    	
    	chat.clear();
    	if (!sortedMessages.isEmpty()){
            for (Entry<Long, Entry<Integer,String>> message : sortedMessages.entrySet()) {
            	if (message.getValue().getKey() == 1)
            		this.chat.add("           " + newFormat.format(message.getKey()) + ": "+senderName+": " + message.getValue().getValue());
            	else 
                    this.chat.add(newFormat.format(message.getKey()) + ": " + name.get() + ": " + message.getValue().getValue());
    		}
    	}else
    		this.chat.clear();
    }
    
    public ObservableMap<Long, String> getMessages(){
    	return messages;
    }
    public ObservableList<String> getChat(){
    	return chat;
    }
    public void SetName(String newName){
    	name= new SimpleStringProperty(newName);
    }
    
    public void setOnline(boolean value) {
      this.online.set(value);
    }
}
