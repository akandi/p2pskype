package gui.model;

public class FriendRequestOutgoing extends Person {

  public FriendRequestOutgoing(String name) {
    super(name);
    this.name.set(rawName + " (unconfirmed)");
  }
}
