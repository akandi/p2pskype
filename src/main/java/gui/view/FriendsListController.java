package gui.view;

import challengetask.application.interfaces.NameChangeCallback;
import gui.MainApp;
import gui.model.FriendRequestIncoming;
import gui.model.Person;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;
import org.controlsfx.control.Notifications;
import org.controlsfx.control.action.Action;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

//import javax.security.auth.callback.Callback;


public class FriendsListController implements Initializable{

    @FXML
    private TableView<Person> friends;
    @FXML
    private TableColumn<Person, Boolean> onlineColumn;
    @FXML
    private TableColumn<Person, String> nameColumn;
    @FXML
    private ListView<String> chatMessagesList = new ListView<String>();

    @FXML
    private Label userNameLabel;
    @FXML
    private Label selectionNameLabel;
    @FXML
    private TextArea message;

    private MainApp application;

    private ObservableList<String> chatMessages = FXCollections.observableArrayList();

    public void initialize(URL location, ResourceBundle resources) {
      //  onlineColumn.setCellValueFactory(cell -> cell.getValue().onlineProperty());
    	onlineColumn.setCellValueFactory(cellData -> cellData.getValue().onlinePropertytext());
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        onlineColumn.setCellFactory(new Callback<TableColumn<Person,Boolean>,TableCell<Person,Boolean>>(){

			@Override
			public TableCell<Person, Boolean> call(TableColumn<Person, Boolean> param) {
				TableCell<Person, Boolean> cell = new TableCell<Person, Boolean>(){
		        	@Override
		        	public void updateItem(Boolean item, boolean empty){
		        		ImageView im=new ImageView();
		        		if (item != null){
		        		if (item.equals(new Boolean(false))){
		        			try {
		        	            im = new ImageView(new Image(getClass().getClassLoader().getResource("css/icons/offline.png").toString()));
		        	        } catch (NullPointerException e) {}
		        		}else{
		        			try {
		        	            im = new ImageView(new Image(getClass().getClassLoader().getResource("css/icons/online.png").toString()));
		        	        } catch (NullPointerException e) {}
		        		}
		        		
		        		setGraphic(im);
		        		}else{
		        			
		        		}
		        			
		        	}
					
				};
				return cell;
			}
        });

        chatMessagesList.setItems(chatMessages);

        showPersonDetails(null);

        // Listen for selection changes and show the person details when changed.
        friends.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue instanceof FriendRequestIncoming) {
                        newValue.click();
                    } else {
                        Platform.runLater(() -> showPersonDetails(newValue));
                    }

                });
    }

    public void setApp(MainApp application) {
        this.application = application;

        // Add observable list data to the table
        Person user = application.getPerson();
        userNameLabel.setText(user.getName());

        friends.setItems(user.getFriends());

        application.getP2pSkype().subscribeNameChange(new NameChangeCallback() {
            @Override
            public void call(String newName) {
                setName(newName);
            }
        });
        
    }

    private void setName(final String name){
        javafx.application.Platform.runLater(() -> userNameLabel.setText(name));
        application.setPersonName(name);
        application.updateAllChat();
    }

    private void showPersonDetails(Person person) {
        if (person != null) {
            selectionNameLabel.setText(person.getName());
            	chatMessages=person.getChat();
            	chatMessagesList.setItems(chatMessages);
        } else {
            selectionNameLabel.setText("");
        }
    }

    @FXML
    private void btnSendAction(ActionEvent event) throws IOException {
        if (!message.getText().equals("")){
        	if (friends.getSelectionModel().selectedItemProperty() != null){
        		application.sendChatMessage(friends.getSelectionModel().selectedItemProperty().get(),message.getText());
        		message.setText("");
        	}
        }
    }
    
    @FXML
    private void listPeersClick(ActionEvent event) throws IOException {
        application.listPeers();
    }
    
    @FXML
    private void addFriendClick(ActionEvent event) throws IOException {
        application.addFriend();
    }

    @FXML
    private void changeNameClick(ActionEvent event) throws IOException {
        TextInputDialog dialog = new TextInputDialog();

        dialog.setTitle("Name Change");
        dialog.setHeaderText("Change your name:");

        Optional<String> result = dialog.showAndWait();

        result.ifPresent(name -> application.getP2pSkype().changeName(name));
    }

    /*@FXML
    private void requestLogClick(ActionEvent event) throws IOException {
        if (friends.getSelectionModel().selectedItemProperty() != null) {
            Person target = friends.getSelectionModel().selectedItemProperty().get();
            application.sendLogfileRequest(target);
            Notifications.create()
                    .title("Requesting LOG...")
                    .text("Establishing connection to " + target.getName())
                    .position(Pos.TOP_RIGHT)
                    .showInformation();
        }
    }*/
    
    @FXML
    private void requestLogfileClick(ActionEvent event) throws IOException {
        application.showRequestLogfile();
    }


    @FXML
    private void videoFriendClick(ActionEvent event) throws IOException {
        if (friends.getSelectionModel().selectedItemProperty() != null) {
            Person target = friends.getSelectionModel().selectedItemProperty().get();
            application.sendVideoRequest(target);
            Notifications.create()
                    .title("Video...")
                    .text("Establishing connection to " + target.getName())
                    .position(Pos.TOP_RIGHT)
                    .showInformation();
        }


    }

    private void showIncomingCall(){
        Notifications.create()
                .title("Incoming call")
                .text("")
                .position(Pos.CENTER)
                .action(new Action("Accept"), new Action("Decline"))
                .showConfirm();
    }
}
