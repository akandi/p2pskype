package gui.view;

import gui.MainApp;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Marc on 20.04.16.
 */
public class LoginController implements Initializable {

    @FXML
    private Label lblMessage;
    @FXML
    private TextField txtUsername;

    private MainApp application;

    public void setApp(MainApp application){
        this.application = application;
    }


    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void btnLoginAction(ActionEvent event) throws IOException {
        application.userLogin(txtUsername.getText());
    }

}
