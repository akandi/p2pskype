package gui.view;

import gui.MainApp;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Marc on 12.05.16.
 */
public class VideoController implements Initializable{

    @FXML
    private ImageView video;
    @FXML
    private Button closeBtn;

    private MainApp application;
    
    private Stage stage;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        /*try {
            Image image = new Image(Paths.get("src/main/java/gui/css/icons/online.png").toUri().toURL().toString());
            video.setImage(image);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }*/
    }

    public void setApp(MainApp application) {
        this.application = application;
    }

    public ImageView getVideoView(){
        return this.video;
    }

    @FXML
    private void terminateClick(ActionEvent event) throws IOException {
      // this will close this window in due course  
      Platform.runLater(new Runnable() {
          @Override
          public void run() {
              application.terminateVideo();
          }
      });
    }

    public void close() {
        stage.hide();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
    
    public void show() {
        stage.show();
    }
}
