package gui.view;

import challengetask.application.interfaces.P2PSkype;
import gui.MainApp;
import gui.model.Peer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Marc on 27.04.16.
 */
public class RequestLogfileController implements Initializable{
    @FXML
    private TableView<Peer> peerListTable;
    @FXML
    private TableColumn<Peer, String> ipColumn;
    @FXML
    private TableColumn<Peer, String> portColumn;
    @FXML
    private TableColumn<Peer, String> versionColumn;

    private MainApp application;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        peerListTable.setRowFactory(view -> {
          TableRow<Peer> row = new TableRow<>();
          row.setOnMouseClicked(item -> peerListItemClick(row.getItem()));
          return row;
        });
      
        ipColumn.setCellValueFactory(cellData -> cellData.getValue().getIp());
        portColumn.setCellValueFactory(cellData -> cellData.getValue().getPort());
        versionColumn.setCellValueFactory(cellData -> cellData.getValue().getVersion());
    }

    public void setApp(MainApp application) {
        this.application = application;

        P2PSkype p2pSkype = application.getP2pSkype();

        ObservableList<Peer> items = FXCollections.observableArrayList();
        for (challengetask.application.interfaces.Peer peer : p2pSkype.listPeers()) {
            items.add(new Peer(peer.getIp(), peer.getPort(), peer.getVersion()));
        }
        peerListTable.setItems(items);
    }
    
    private void peerListItemClick(Peer peer) {        
        application.requestLogfile(peer);
        peerListTable.getScene().getWindow().hide();
    }
}
