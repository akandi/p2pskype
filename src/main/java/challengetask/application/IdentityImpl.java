package challengetask.application;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import challengetask.application.interfaces.Identity;
import net.tomp2p.peers.PeerAddress;

public class IdentityImpl implements Identity, Serializable {
  private static final long serialVersionUID = 3105882288099027100L;

  private String name = null;
  private String id = null;

  private List<String> friends = new LinkedList<String>();
  private List<String> friendRequests = new LinkedList<String>();

  private boolean online = false;
  private PeerAddress lastPeerAddress = null;

  @Override
  public String toString() {
    return name + "<" + id + ">";
  }

  public IdentityImpl(String name, String id) {
    this.name = name;
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public String getId() {
    return id;
  }

  public List<String> getFriendRequests() {
    return friendRequests;
  }

  public List<String> getFriends() {
    return friends;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setOnlineStatus(boolean value) {
    online = value;
  }

  public boolean getOnlineStatus() {
    return online;
  }

  public void setLastPeerAddress(PeerAddress lastPeerAddress) {
    this.lastPeerAddress = lastPeerAddress;
  }

  public PeerAddress getLastPeerAddress() {
    return lastPeerAddress;
  }
}
