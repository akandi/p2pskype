package challengetask.application;

import com.github.sarxos.webcam.Webcam;
import java.awt.Dimension;

import javafx.scene.image.ImageView;
import net.tomp2p.audiovideowrapper.audiovideo.AudioData;
import net.tomp2p.audiovideowrapper.audiovideo.H264Wrapper;
import net.tomp2p.audiovideowrapper.audiovideo.OpusWrapper;
import net.tomp2p.audiovideowrapper.audiovideo.VideoData;
import net.tomp2p.peers.PeerAddress;

public class VideoCall {

  private final static int AUDIO_PORT = 10560;
  private final static int VIDEO_PORT = 10568;

  private AudioData frameAudio;
  private VideoData frameVideo;

  private Webcam webcam;
  
  private boolean receiving = false;
  private boolean haveVideo = false;
  
  private void initWebcam() {
    webcam = Webcam.getDefault();
    Dimension[] d = webcam.getViewSizes();
    webcam.setViewSize(d[d.length - 1]);
  }

  public VideoCall runReceiver(ImageView imgView) throws Exception {
    receiving = true;
    
    // read from udp socket and play
    frameAudio = OpusWrapper.decodeAndPlay(OpusWrapper.FORMAT);
    OpusWrapper.readFromSocket(frameAudio, AUDIO_PORT);
    frameVideo = H264Wrapper.decodeAndPlay(imgView);
    H264Wrapper.readFromSocket(frameVideo, VIDEO_PORT);
    
    return this;
  }
  
  public VideoCall runSender(PeerAddress target) throws Exception {
    receiving = false;

    // read from file and stream across the net
    try {
      frameAudio = OpusWrapper.send(target.inetAddress(), AUDIO_PORT);
    } catch (Exception e) { e.printStackTrace(); }
    try {
      OpusWrapper.recordAndEncode(OpusWrapper.FORMAT, frameAudio);
    } catch (Exception e) { e.printStackTrace(); }
    try {
      initWebcam();
      frameVideo = H264Wrapper.send(target.inetAddress(), VIDEO_PORT);
      H264Wrapper.recordAndEncode(webcam, frameVideo);
      haveVideo = true;
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    return this;
  }

  /*@Override
  public void start(Stage primaryStage) {
    primaryStage.setTitle("Audio Video Test");
    StackPane root = new StackPane();
    root.getChildren().add(IMG);
    while (H264Wrapper.getH() == 0) {
      try {
        Thread.sleep(10);
      } catch (InterruptedException ex) {
        ex.printStackTrace();
      }
    }
    primaryStage.setScene(new Scene(root, H264Wrapper.getW(), H264Wrapper.getH()));
    primaryStage.show();

  }*/

  public void shutdown() {
    if (!receiving) {
      OpusWrapper.stopRecordAndEncode();
      if (haveVideo) {
        H264Wrapper.stopRecordAndEncode();
      }
    }

    try {
      frameAudio.close();      
    } catch (Exception e) {
      // ignore
    }
    try {
      frameVideo.close();      
    } catch (Exception e) {
      // ignore
    }
  }
}
