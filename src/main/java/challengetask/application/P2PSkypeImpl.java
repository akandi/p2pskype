package challengetask.application;

import challengetask.application.interfaces.*;
import challengetask.application.util.NotLoggedInException;
import challengetask.application.util.UserNotFoundException;
import challengetask.message.MessageLog;
import challengetask.message.MessageVideoTermination;
import challengetask.p2p.Conversation;
import challengetask.p2p.P2PNode;
import javafx.util.Callback;
import net.tomp2p.peers.PeerAddress;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class P2PSkypeImpl implements P2PSkype {
  
  // TODO: add a name cache where the gui can look up user names by id without having to request from the dht network every time

  private P2PNode p2pNode;
  
  private Identity p2pIdentity = null;
  private Lock p2pIdentityLock = new ReentrantLock();
  
  private Set<String> friendRequests = new HashSet<String>();
  
  private FriendsListChangedCallback friendsListChangedCallback;
  private FriendRequestCallback friendRequestCallback;
  private NameChangeCallback nameChangeCallback;
  private ConversationChangedCallback conversationChangedCallback;
  private OnlineStatusChangedCallback onlineStatusChangedCallback;
  private IncomingVideoCallCallback incomingVideoCallCallback;
  private VideoCallCallback videoCallCallback;
  private VideoCallCallback videoCallCallbackIncoming;
  private Timer timer;
  
  private final Object lock = new Object();
  private Boolean accepted = null;
  
  public P2PSkypeImpl() {
    try {
      p2pNode = new P2PNode();
      
      // used for the duration of the call
      videoCallCallbackIncoming = new VideoCallCallback() {
        @Override
        public void setAccept(Boolean accept) {
          if (accept == null) accept = false;          
          
          synchronized (lock) {
            accepted = accept;
            lock.notify();
          }
          
          if (accept) {
            try {                
              // TODO investigate whether we can only start sending once the other guy is receiving
              sender = new VideoCall().runSender(peerAddress);
              receiver = new VideoCall().runReceiver(windowCallback.createWindow());
            } catch (Exception e) {
              e.printStackTrace();
            }
          }
        }

        @Override
        public void terminate() {   
          onCloseWindow.call(null);
          sender.shutdown();
          receiver.shutdown();
        }

        @Override
        public void terminateSelfAndOther() {
          try {
            p2pNode.sendMessage(peerAddress, new MessageVideoTermination());
          } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
          }
          terminate();
        }
      };
      
      videoCallCallback = videoCallCallbackIncoming;
            
      // called from the p2pnode
      Callback<PeerAddress, Boolean> innerVideoCallback = new Callback<PeerAddress, Boolean>() {                
        @Override
        public Boolean call(PeerAddress peerAddress) {  
          accepted = null;
          
          videoCallCallback = videoCallCallbackIncoming;
          videoCallCallback.setPeerAddress(peerAddress);
          
          new Thread(new Runnable() {
            @Override
            public void run() {
              incomingVideoCallCallback.handleIncomingCall(videoCallCallback);
            }           
          }).start();
          
          while (accepted == null) {
            synchronized (lock) {
              try {
                lock.wait();
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          }
          
          return accepted;
        }       
      };
      
      Callback<Void, Void> videoTerminationCallback = new Callback<Void, Void>() {
        public Void call(Void param) {
          videoCallCallback.terminate();
          return null;
        }
      };
            
      p2pNode.subscribeIncomingCall(innerVideoCallback);
      p2pNode.subscribeVideoTermination(videoTerminationCallback);
      p2pNode.connect();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  @Override
  public boolean tryLogin(String name) {
    if (name.equals("")) return false;
    
    setP2PIdentity(p2pNode.searchIdentity(name));
    if (p2pIdentity == null) {
      try {
        setP2PIdentity(new IdentityImpl("Default", name));
        p2pIdentityLock.lock();
        Future<Boolean> future = p2pNode.putOwnIdentity(p2pIdentity);
        p2pIdentityLock.unlock();
        if (!future.get()) {
          setP2PIdentity(null);
        }
      } catch (IOException | InterruptedException | ExecutionException e) {
        setP2PIdentity(null);
        e.printStackTrace();
      }
    }
    
    if (p2pIdentity != null) {
    	try {
    	  p2pIdentityLock.lock();
    		p2pIdentity.setOnlineStatus(true);
    		p2pIdentityLock.unlock();
			p2pNode.putOwnIdentity(p2pIdentity);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    //  p2pNode.setOnlineStatus(p2pIdentity, true);
      
      // regularly update identity
    	Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {

        @Override
        public void run() {
          while (p2pNode.isWaitingIdentity() || p2pNode.isWaitingChat()) {
            try {
              Thread.sleep(100);
            } catch (InterruptedException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
          }
          String id = p2pIdentity.getId();
          setP2PIdentity(p2pNode.searchIdentity(id));
          while (p2pIdentity == null)
            setP2PIdentity(p2pNode.searchIdentity(id));

          if (nameChangeCallback != null)
            nameChangeCallback.call(p2pIdentity.getName());
          if (friendsListChangedCallback != null)
            handleFriendListChanges();
          if (friendRequestCallback != null) {
            if (p2pIdentity.getFriendRequests().size() > 0) {
              handleNewFriendRequests(p2pIdentity.getFriendRequests());
            }
          }
          if (conversationChangedCallback != null) {
            handleConversationChanged();
          }
          if (onlineStatusChangedCallback != null) {
            handleOnlineStatusChanged();
          }
        }

      }, 0, 5, TimeUnit.SECONDS);
      
      // check friends still online 
    	Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {

        @Override
        public void run() {
          // every 2 minutes
          // contact friends to see if they're online
          // if not, also change their online status in the dht
          String id = p2pIdentity.getId();
          setP2PIdentity(p2pNode.searchIdentity(id));
          while (p2pIdentity == null)
            setP2PIdentity(p2pNode.searchIdentity(id));

          for (String friendId : p2pIdentity.getFriends()) {
            Identity friendIdent = p2pNode.searchIdentity(friendId);
            boolean online;
            try {
              online = p2pNode.directOnlineStatusCheck(friendId);
              if (friendIdent.getOnlineStatus() != online) {
                System.out.println("not online ======" + friendId);
                p2pNode.setOnlineStatus(friendIdent, online);
                // the ui callback will happen the next time the friends online
                // status is fetched from the dht
              }
            } catch (ClassNotFoundException | IOException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
          }
        }

      }, 0, 2, TimeUnit.MINUTES);
      
      return true;
    } else {
      return false;
    }
    
  }

  protected void handleOnlineStatusChanged() {
    for (String friendId : p2pIdentity.getFriends()) {
      Identity friendIdent = p2pNode.searchIdentity(friendId);
      System.out.println("check online:"+friendId);
      onlineStatusChangedCallback.update(friendId, friendIdent.getOnlineStatus());
    }
  }

  private void handleFriendListChanges() {
    List<User> friends = new LinkedList<User>();
    for (String friendId : p2pIdentity.getFriends()) {
      Identity friendIdentity = p2pNode.searchIdentity(friendId);
      
      if (friendIdentity != null) {
        friends.add(new UserImpl(friendId, friendIdentity.getName()));
      }
    }
    
    friendsListChangedCallback.call(friends);
  }

  private void handleConversationChanged(){
	  for (String id:p2pIdentity.getFriends()){
		  Conversation conversation = p2pNode.searchConversation(p2pIdentity.getId(),id);
		  if (conversation != null){
			  System.out.println("chat callout"+conversation.getChat().size());
				  conversationChangedCallback.callOut(id, conversation.getChat());
		  }
		  Conversation conversation2 = p2pNode.searchConversation(id,p2pIdentity.getId());
		  if (conversation2 != null){
			  System.out.println("chat callin"+conversation2.getChat().size());
		    conversationChangedCallback.callIn(id, conversation2.getChat());
		  }
		  
		  
	  }
	  
  }

  private void handleNewFriendRequests(List<String> requests) {
    for (String friendId : requests) {
      if (!friendRequests.contains(friendId)) {
        Identity friend = p2pNode.searchIdentity(friendId);
        if (friend != null) {
          friendRequests.add(friendId);
          
          // handling the request waits for user input, thus we start it in its own thread
          new Thread(new Runnable() {
            @Override
            public void run() {
              boolean result = friendRequestCallback.displayRequest(new UserImpl(friend.getId(), friend.getName()));
              
              for (String requestId : p2pIdentity.getFriendRequests()) {
                if (requestId.equals(friend.getId())) {
                  p2pIdentityLock.lock();
                  p2pIdentity.getFriendRequests().remove(requestId);
                  p2pIdentityLock.unlock();
                }
              }
              if (result) {                
                p2pIdentityLock.lock();
                p2pIdentity.getFriends().add(friendId);
                p2pIdentityLock.unlock();
                Identity friendIdentity = p2pNode.searchIdentity(friendId);
                friendIdentity.getFriends().add(p2pIdentity.getId());
                try {                  
                  p2pNode.putIdentity(friendIdentity);
                } catch (IOException e) {
                  e.printStackTrace();
                }
              }
              
              try {                  
                p2pNode.putOwnIdentity(p2pIdentity);
              } catch (IOException e) {
                e.printStackTrace();
              }
                            
              friendRequests.remove(friendId);
            }            
          }).start();          
        }
      }
    }
  }

  @Override
  public User sendFriendRequest(String name) throws NotLoggedInException, UserNotFoundException, IOException {
    if (p2pIdentity == null) throw new NotLoggedInException("Must be logged in to send friend request");
    
    Identity friend = p2pNode.searchIdentity(name);
    if (friend == null) throw new UserNotFoundException("user not found");
    
    p2pNode.sendFriendRequest(friend, p2pIdentity);
    
    return new UserImpl(friend.getId(), friend.getName());
  }

  @Override
  public List<Peer> listPeers() {
    return p2pNode.getPeers();
  }

  @Override
  public void subscribeFriendsListChanged(FriendsListChangedCallback cb) {
    friendsListChangedCallback = cb;
  }
  
  @Override
  public void subscribeConversationChanged(ConversationChangedCallback cb){
	  
	  conversationChangedCallback=cb;
  }

  @Override
  public void subscribeFriendRequest(FriendRequestCallback cb) {
    this.friendRequestCallback = cb;
  }

  @Override
  public void changeName(String name) {
	  if (p2pIdentity == null)
		  return;
	  p2pIdentityLock.lock();
    p2pIdentity.setName(name);
    p2pIdentityLock.unlock();
    try {
      p2pIdentityLock.lock();
      p2pNode.putOwnIdentity(p2pIdentity);
      p2pIdentityLock.unlock();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  @Override
  public void subscribeNameChange(NameChangeCallback cb) {
    nameChangeCallback = cb;
  }

  @Override
  public void sendChatMessage(String receiverId,String message)  {
	  try {
		  p2pNode.sendChatMessage(p2pIdentity.getId(), receiverId, message);
	  } catch (IOException | ClassNotFoundException e) {
	      e.printStackTrace();
	    } 
  }

  @Override
  public VideoCallCallback sendVideoRequest(String id) {
    accepted = null;
    
    videoCallCallback = new VideoCallCallback() {
      @Override
      public void setAccept(Boolean accept) {
        if (accept == null) {
          try {
            p2pNode.sendChatMessage(p2pIdentity.getId(), id, "missed call");
          } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
          }
          return;
        }
        
        if (accept) {
          try {
            // TODO investigate whether we can only start sending once the other guy is receiving
            sender = new VideoCall().runSender(peerAddress);
            receiver = new VideoCall().runReceiver(windowCallback.createWindow());
          } catch (Exception e) {
            e.printStackTrace();
          }
        } else {
          if (onDenied != null) {
            onDenied.call(null);
          }
        }
      }

      @Override
      public void terminate() {
        try {
            onCloseWindow.call(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            sender.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            receiver.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
      }

      @Override
      public void terminateSelfAndOther() {
        try {
          p2pNode.sendMessage(peerAddress, new MessageVideoTermination());
        } catch (ClassNotFoundException | IOException e) {
          e.printStackTrace();
        }
        terminate();
      }
    };

    new Thread(new Runnable() {
      @Override
      public void run() {
        p2pNode.sendVideoRequest(id, videoCallCallback);
      }
    }).start();

    return videoCallCallback;
  }

	@Override
	public void sendLogfileRequest(String ip, String port, final Future<File> fileDialogFuture) {
	  new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          MessageLog log = p2pNode.sendLogfileRequest(ip, new Integer(port));
          File file = fileDialogFuture.get();
          if (log != null && file != null) { 
            try (FileOutputStream fos = new FileOutputStream(file)) {
              BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
  
              for (String logEntry : log.getEntries()) {
                // ToDo [NiceToHave]: Filter out log-entries that don't fit a requested log-level
                bw.write(logEntry);
                bw.newLine();
              }
  
              bw.flush();
              bw.close();
            }
            catch (Exception e) {
              System.err.println(e);
            }
          }
        } catch (NumberFormatException | ClassNotFoundException | IOException | InterruptedException | ExecutionException e) {
          e.printStackTrace();
        }
      }
	  }).start();
	}

  @Override
  public void subscribeOnlineStatusChanged(OnlineStatusChangedCallback cb) {
    onlineStatusChangedCallback = cb;
  }
	
	@Override
	public void shutdown(){
	  Future<Boolean> future = p2pNode.setOnlineStatus(p2pIdentity, false);
	  try {
      future.get();
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }
	  
		if (timer != null)
			timer.cancel();
		if (p2pNode != null)
			p2pNode.shutdown();
		
	}

  @Override
  public void subscribeIncomingVideoCall(IncomingVideoCallCallback incomingVideoCallCallback) {
    this.incomingVideoCallCallback = incomingVideoCallCallback;
  }
  
  private void setP2PIdentity(Identity newId) {
    p2pIdentityLock.lock();
    p2pIdentity = newId;
    p2pIdentityLock.unlock();
  }
}
