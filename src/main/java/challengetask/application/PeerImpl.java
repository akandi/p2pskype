package challengetask.application;

import challengetask.application.interfaces.Peer;

public class PeerImpl implements Peer {
  private String ip;
  private String port;
  private String version;
  
  public PeerImpl(String ip, String port, String version) {
    this.ip = ip;
    this.port = port;
    this.version = version;
  }
  
  @Override
  public String getIp() {
    return ip;
  }

  @Override
  public String getPort() {
    return port;
  }

  @Override
  public String getVersion() {
    return version;
  }

  @Override
  public void setVersion(String version) {
    this.version = version;
  }
}
