package challengetask.application.interfaces;

public interface User {
  public String getName();
  public String getId();
}
