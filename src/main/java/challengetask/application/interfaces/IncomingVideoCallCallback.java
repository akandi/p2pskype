package challengetask.application.interfaces;

public interface IncomingVideoCallCallback {
  public void handleIncomingCall(VideoCallCallback cb);
}
