package challengetask.application.interfaces;

import challengetask.application.util.NotLoggedInException;
import challengetask.application.util.UserNotFoundException;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Future;

public interface P2PSkype {
  public boolean tryLogin(String name);
  
  public User sendFriendRequest(String name) throws NotLoggedInException, UserNotFoundException, IOException;
  
  public List<Peer> listPeers();
  
  public void subscribeFriendsListChanged(FriendsListChangedCallback cb);
  
  // this callback is allowed to block until a decision is reached
  public void subscribeFriendRequest(FriendRequestCallback cb);

  public void subscribeNameChange(NameChangeCallback cb);

  public void changeName(String name);

  public void subscribeConversationChanged(ConversationChangedCallback cb);

  public void sendChatMessage(String receiverId,String message);

  public VideoCallCallback sendVideoRequest(String receiver);

  public void sendLogfileRequest(String ip, String port, Future<File> fileDialogFuture);
  
  public void shutdown();
  
  public void subscribeOnlineStatusChanged(OnlineStatusChangedCallback cb);

  public void subscribeIncomingVideoCall(IncomingVideoCallCallback incomingVideoCallCallback);
}
