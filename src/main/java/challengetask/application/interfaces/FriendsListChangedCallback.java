package challengetask.application.interfaces;

import java.util.List;

public interface FriendsListChangedCallback {
    public void call(List<User> friends);
}
