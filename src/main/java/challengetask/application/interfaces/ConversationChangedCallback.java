package challengetask.application.interfaces;

import java.util.SortedMap;

public interface ConversationChangedCallback {
	public void callOut(String receiverId,SortedMap<Long, String> newMessages);
	public void callIn(String receiverId,SortedMap<Long, String> newMessages);
}
