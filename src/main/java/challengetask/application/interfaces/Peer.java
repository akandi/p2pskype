package challengetask.application.interfaces;

public interface Peer {
  public String getIp();
  public String getPort();
  public String getVersion();
  public void setVersion(String version);
}
