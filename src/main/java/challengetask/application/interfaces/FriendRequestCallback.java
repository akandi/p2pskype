package challengetask.application.interfaces;

// this callback is allowed to block until a decision is reached
public interface FriendRequestCallback {
  public boolean displayRequest(User user);
  
  public void requestResolved(boolean decision);
}
