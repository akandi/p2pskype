package challengetask.application.interfaces;

import challengetask.application.VideoCall;
import javafx.util.Callback;
import net.tomp2p.peers.PeerAddress;

public abstract class VideoCallCallback {
    protected VideoWindowCallback windowCallback = null;
    protected PeerAddress peerAddress = null;
    protected Callback<Void, Void> onDenied = null;
    protected Callback<Void, Void> onCloseWindow = null;    
    
    protected VideoCall sender = null;
    protected VideoCall receiver = null;
  
    // let's define null as no answer, peer offline, ...
    public abstract void setAccept(Boolean accept);
    
    public void setPeerAddress(PeerAddress peerAddress) {
        this.peerAddress = peerAddress;
    }

    public void onRequireVideoWindow(VideoWindowCallback videoWindowCallback) {
        windowCallback = videoWindowCallback;
    }

    public void setOnDenied(Callback<Void, Void> callback) {
        onDenied = callback;
    }
    
    public abstract void terminate();
    
    public abstract void terminateSelfAndOther();

    public void onCloseWindow(Callback<Void, Void> cb) {
        onCloseWindow = cb;
    }
}
