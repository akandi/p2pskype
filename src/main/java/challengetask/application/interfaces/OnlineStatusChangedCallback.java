package challengetask.application.interfaces;

public interface OnlineStatusChangedCallback {
  public void update(String personId, boolean online);
}
