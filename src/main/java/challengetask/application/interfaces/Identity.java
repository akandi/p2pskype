package challengetask.application.interfaces;

import java.util.List;

import net.tomp2p.peers.PeerAddress;

public interface Identity {
  String getName();
  String getId();
  List<String> getFriendRequests();
  List<String> getFriends();
  void setName(String name);
  void setOnlineStatus(boolean value);
  boolean getOnlineStatus();
  void setLastPeerAddress(PeerAddress lastPeerAddress);
  PeerAddress getLastPeerAddress();
}
