package challengetask.application.interfaces;

import javafx.scene.image.ImageView;

public interface VideoWindowCallback {
  public ImageView createWindow();
}
