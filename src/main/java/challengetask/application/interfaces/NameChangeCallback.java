package challengetask.application.interfaces;

public interface NameChangeCallback {
  public void call(String newName);
}
