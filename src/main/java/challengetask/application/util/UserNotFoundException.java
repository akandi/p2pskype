package challengetask.application.util;

public class UserNotFoundException extends Exception {
  private static final long serialVersionUID = 1972661121302574835L;

  public UserNotFoundException(String str) {
    super(str);
  }
}
