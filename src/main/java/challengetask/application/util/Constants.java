package challengetask.application.util;

public class Constants {
  public static final String VERSION_NUMBER = "3";
  public static final String BOOTSTRAP_IP = "188.166.39.67";
  //public static final String BOOTSTRAP_IP = "127.0.0.1";
  public static final int BOOTSTRAP_PORT = 9512;
  
  // in our app a video frame is usually around 32 KB, thus the VIDEO_SLEEP_AFTER_PACKET has to be kept so low that 
  // (32000 / VIDEO_PACKET_PAYLOAD_SIZE) * VIDEO_SLEEP_AFTER_PACKET is below 1/10 sec. Otherwise the video encoder output
  // will just choke the channel
  public static final int VIDEO_SLEEP_AFTER_PACKET = 0;
  public static final int VIDEO_PACKET_PAYLOAD_SIZE = 200;
}
