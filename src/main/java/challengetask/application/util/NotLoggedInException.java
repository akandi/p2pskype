package challengetask.application.util;

public class NotLoggedInException extends Exception {
  private static final long serialVersionUID = 7161396842999961223L;

  public NotLoggedInException(String str) {
    super(str);
  }
}
