package challengetask.application.util;

public class UnhandleableInconsistencyException extends Exception {
  private static final long serialVersionUID = 1546718655967996948L;

  public UnhandleableInconsistencyException(String msg) {
    super(msg);
  }
}
