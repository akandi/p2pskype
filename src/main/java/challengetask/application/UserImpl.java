package challengetask.application;

import challengetask.application.interfaces.User;

public class UserImpl implements User {

  private String name;
  private String id;
  
  public UserImpl(String id, String name) {
    this.name = name;
    this.id = id;
  }
  
  @Override
  public String getName() {
    return name;
  }
  
  @Override 
  public String getId() {
    return id;
  }
}
