package challengetask.p2p;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.FutureTask;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import challengetask.application.IdentityImpl;
import challengetask.application.interfaces.Identity;
import org.slf4j.LoggerFactory;

import challengetask.application.interfaces.VideoCallCallback;
import challengetask.application.util.Constants;
import challengetask.application.util.UnhandleableInconsistencyException;
import challengetask.message.Message;
import challengetask.message.MessageLog;
import challengetask.message.MessageLogRequest;
import challengetask.message.MessageVersion;
import challengetask.message.MessageVersionRequest;
import challengetask.message.MessageVideo;
import challengetask.message.MessageVideoRequest;
import challengetask.message.MessageVideoTermination;
import challengetask.message.OnlineStatusRequest;
import challengetask.message.OnlineStatusResponse;
import javafx.util.Callback;
import net.tomp2p.connection.Bindings;
import net.tomp2p.connection.ChannelCreator;
import net.tomp2p.connection.DefaultConnectionConfiguration;
import net.tomp2p.dht.FutureGet;
import net.tomp2p.dht.FuturePut;
import net.tomp2p.dht.PeerBuilderDHT;
import net.tomp2p.dht.PeerDHT;
import net.tomp2p.futures.*;
import net.tomp2p.nat.FutureNAT;
import net.tomp2p.nat.FutureRelayNAT;
import net.tomp2p.nat.PeerBuilderNAT;
import net.tomp2p.nat.PeerNAT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.p2p.PeerBuilder;
import net.tomp2p.peers.Number160;
import net.tomp2p.peers.PeerAddress;
import net.tomp2p.relay.tcp.TCPRelayClientConfig;
import net.tomp2p.replication.IndirectReplication;
import net.tomp2p.rpc.ObjectDataReply;
import net.tomp2p.storage.Data;
import net.tomp2p.peers.Number640;
import net.tomp2p.utils.Pair;

public class P2PNode {
  private static final ch.qos.logback.classic.Logger LOG = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(P2PNode.class);
  
  private final Random RND = new Random();
  
  private Peer peer = null;
  private PeerDHT peerDHT = null;
  private Bindings bindings = null;
  
  private List<Message> messageList=new ArrayList<Message>();
  
  private Callback<PeerAddress, Boolean> incomingVideoCallCallback;
  private Callback<Void, Void> videoTerminationCallback;
  
  boolean waitingIdentity=false;
  boolean waitingChat=false;
  
  /**
   * returns a random port in the range 7000 to 7999
   */
  private int randomPort() {
    return RND.nextInt(1000) + 7000;
	  //return 9512;
  }
  
  public void connect() throws Exception {
    connect(Constants.BOOTSTRAP_IP, Constants.BOOTSTRAP_PORT);
  }
  
  void connect(String bootstrapIp, int bootstrapPort) throws Exception {
    LOG.debug("Starting node");
    bindings = new Bindings().listenAny();
    
    peer = new PeerBuilder(new Number160(RND)).ports(randomPort()).bindings(bindings).start();
    final PeerNAT peerNAT = new PeerBuilderNAT(peer).start();
    
    final FutureDiscover futureDiscover = peer.discover().inetAddress(Inet4Address.getByName(bootstrapIp)).
        ports(bootstrapPort).start();

    futureDiscover.awaitUninterruptibly();
    LOG.debug("futureDiscover complete " + futureDiscover.isSuccess());
    
    if (futureDiscover.isFailed()) {
      final FutureNAT fnat = peerNAT.startSetupPortforwarding(futureDiscover);
      fnat.awaitUninterruptibly();
      
      LOG.debug("fnat complete " + fnat.isSuccess());
      
      if (fnat.isFailed()) {
        FutureRelayNAT frn = peerNAT.startRelay(new TCPRelayClientConfig(), futureDiscover, fnat);
        frn.awaitUninterruptibly();
               
        LOG.debug("frn complete " + frn.isSuccess());
        
        if (frn.isFailed()) {
          LOG.warn("Can't connect to bootstrap node. Either unable to get NAT or relay to work or we are the bootstrap node coming up");
          
          // either the bootstrap node is down or we are it
          // start bootstrap node here
          // if we run under the bootstrap ip other nodes will be able to connect to us,
          // otherwise we run a bootstrap node that cannot be found
          
          LOG.debug("Starting bootstrap node");
          BaseFuture baseFuture = peer.shutdown();
          baseFuture.awaitUninterruptibly();
          startBootstrap(bootstrapPort);
        } else {
          continueNormalNode(bootstrapIp, bootstrapPort);
        }        
      } else {
        LOG.debug("Found that my outside address is " + fnat.peerAddress());
        continueNormalNode(bootstrapIp, bootstrapPort);
      }
    } else {
      LOG.debug("Found that my outside address is " + futureDiscover.peerAddress());
      continueNormalNode(bootstrapIp, bootstrapPort);
    }
    startMessageListener();
  }
  
  private void startBootstrap(int bootstrapPort) throws Exception {
    peer = new PeerBuilder(new Number160(RND)).ports(bootstrapPort).bindings(bindings).start();
    new PeerBuilderNAT(peer).start();
    
    // LOG.info("Server started Listening to: " + DiscoverNetworks.discoverInterfaces(bindings));
    LOG.debug("Address visible to outside is " + peer.peerAddress());
    
    peerDHT = new PeerBuilderDHT(peer).start();
    new IndirectReplication(peerDHT).start();
    
    new Thread(new Runnable() {
      @Override
      public void run() {
        while (true) {
          for (PeerAddress pa : peer.peerBean().peerMap().all()) {
            LOG.debug("PeerAddress: " + pa);

            FutureChannelCreator fcc = peer.connectionBean().reservation().create(1, 1);
            fcc.awaitUninterruptibly();

            ChannelCreator cc = fcc.channelCreator();

            FutureResponse fr1 = peer.pingRPC().pingTCP(pa, cc, new DefaultConnectionConfiguration());
            fr1.awaitUninterruptibly();

            if (fr1.isSuccess()) {
              LOG.debug("peer online T:" + pa);
            } else {
              LOG.debug("offline " + pa);
            }

            FutureResponse fr2 = peer.pingRPC().pingUDP(pa, cc, new DefaultConnectionConfiguration());
            fr2.awaitUninterruptibly();

            cc.shutdown();

            if (fr2.isSuccess()) {
              LOG.debug("peer online U:" + pa);
            } else {
              LOG.debug("offline " + pa);
            }
          }
          try {
            Thread.sleep(1500);
          } catch (InterruptedException e) {
            // ignore
          }
        }
      }
    }).start();
  }
  
  private void continueNormalNode(String bootstrapIp, int bootstrapPort) throws Exception {
    LOG.debug("Continuing as normal node");
    
    // LOG.info("Listening to: " + DiscoverNetworks.discoverInterfaces(bindings));
    LOG.debug("address visible to outside is " + peer.peerAddress());
    
    // Future Bootstrap - slave
    FutureBootstrap futureBootstrap = peer.bootstrap().inetAddress(Inet4Address.getByName(bootstrapIp)).ports(bootstrapPort).start();
    futureBootstrap.awaitUninterruptibly();
    
    if (futureBootstrap.isFailed()) {
      LOG.warn(futureBootstrap.failedReason());
    }
    
    peerDHT = new PeerBuilderDHT(peer).start();
    new IndirectReplication(peerDHT).start();

    LOG.debug("" + peer.peerBean().peerMap().all().size());
  }
  
  private static interface GetVersionComplete {
    public void setVersion(String version);
  }

  private void getVersion(PeerAddress neighbor, GetVersionComplete callback) {
    try {
      MessageVersion mv = (MessageVersion)sendMessage(neighbor, new MessageVersionRequest());
      
      callback.setVersion(mv.getVersion());
    } catch (ClassNotFoundException | IOException e) {
      e.printStackTrace();
    }
  }

  private PeerDHT getPeerDHT() {    
    return peerDHT;
  }
  
  public Identity searchIdentity(String id) {
    try {
      FutureGet fget = peerDHT.get(Number160.createHash(id)).getLatest().start();
      
      fget.awaitUninterruptibly();
      for (int i=0;i<4 && !fget.isSuccess();i++){
    	  fget = peerDHT.get(Number160.createHash(id)).getLatest().start();
          fget.awaitUninterruptibly();
      }
      
      if (fget.isSuccess()) {
        Data tmp = fget.data();
        if (tmp != null) {
          return (IdentityImpl)tmp.object();
        }
      }
      
      return null;
    } catch (ClassNotFoundException | IOException e) {
      return null;
    }
  }
  
  public Future<Boolean> putOwnIdentity(Identity identity) throws IOException {
    identity.setLastPeerAddress(peer.peerAddress());
    
    return putIdentity(identity);
  }

  public Future<Boolean> putIdentity(Identity identity) throws IOException {
  	while (isWaitingIdentity()){
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	  LOG.debug("storing identity " + identity.getId());
    FutureTask<Boolean> res = new FutureTask<Boolean>(new Callable<Boolean>() {
      @Override 
      public Boolean call() {
        try {
          LOG.debug("storing identity inside futuretask" + identity.getId());
          boolean res=false;
          res = store(getPeerDHT(), identity, Number160.createHash(identity.getId()));
          for (int i=0;i<30 && (res) ==false;i++){
        	  waitingIdentity=true;
        	  Thread.sleep(2000);
        	  res = store(getPeerDHT(), identity, Number160.createHash(identity.getId()));
          }
          waitingIdentity=false;
          LOG.debug("store returned " + res + " for identity " + identity.getId());
          return res;
        } catch (UnhandleableInconsistencyException e) {
          LOG.debug("caught unhandleableinconsistencyexception for identity " + identity.getId());
          Identity id = searchIdentity(identity.getId());
          if (id == null) {
            // we're putting a new identity and chances are noone is going to want to take the same identity at the same time
            try {
              FuturePut future = peerDHT.put(Number160.createHash(identity.getId())).data(new Data(identity)).start();
              future.awaitUninterruptibly();
              LOG.debug("simple put successful for identity " + identity.getId());
              
              return true;
            } catch (IOException e1) {
              e1.printStackTrace();
            }
            
            LOG.debug("simple put failed for identity " + identity.getId());
          } else {
            // TODO maybe handle
            e.printStackTrace();
          }         
        } catch (ClassNotFoundException | InterruptedException | IOException e) {
          e.printStackTrace();
        }
        
        return false;
      }
    });
    
    Executors.newSingleThreadExecutor().execute(res);
    return res;
  }
  
/*********************************/
  
  private static boolean store(PeerDHT peerDHT, Object object, Number160 hash)
      throws ClassNotFoundException, InterruptedException, IOException, UnhandleableInconsistencyException {
    Pair<Number640, Byte> pair2 = null;

    Pair<Number160, Data> pair = getAndUpdate(peerDHT, object, hash);
    if (pair == null) {
      return false;
    }
    FuturePut fp = peerDHT
        .put(hash)
        .data(Number160.ZERO, pair.element1().prepareFlag(),
            pair.element0()).start().awaitUninterruptibly();
    pair2 = checkVersions(fp.rawResult());
    System.out.println("put ZERO ---------------" );
    Pair<Number160, Data> pair3=getAndUpdate(peerDHT, object, hash);
    for (int i=0; i<10 &&pair3==null;i++){
     pair3 = getAndUpdate(peerDHT, object, hash);
    }
    
    // 1 is PutStatus.OK_PREPARED
    if (!(pair2 != null && pair2.element1() == 1) && (pair3==null)) {
      LOG.debug("have to waitt before inserting identity");
      // if not removed, a low ttl will eventually get rid of it
      peerDHT.remove(hash).versionKey(pair.element0()).start()
          .awaitUninterruptibly();
      return false;
    }
    
    if ((pair2 != null && pair2.element1() == 1) || pair3!=null) {
      fp = peerDHT.put(hash)
          .versionKey(pair2.element0().versionKey()).putConfirm()
          .data(new Data()).start().awaitUninterruptibly();
    } else {
      throw new UnhandleableInconsistencyException("have to handle this manually");
    }
    
    return true;
  }

  //get the latest version and do modification. In this case, append the string
  private static Pair<Number160, Data> getAndUpdate(PeerDHT peerDHT,
      Object object, Number160 hash) throws InterruptedException, ClassNotFoundException,
      IOException, UnhandleableInconsistencyException {
    try {
      Pair<Number640, Data> pair = null;
      
      FutureGet fg = peerDHT.get(hash).getLatest().start()
          .awaitUninterruptibly();
    	System.out.println("fg"+fg);
      // check if all the peers agree on the same latest version, if not
      // wait a little and try again
      pair = checkVersions(fg.rawData());
    	System.out.println("pair"+pair);
      if (pair == null ) {
        LOG.debug("have to wait before inserting identity");
        return null;
      } else {         
        // we got the latest data
    	  System.out.println("pair1-");

        // update operation is replace
        Data newData = new Data(object);
  	  System.out.println("pair2-");
        Number160 v = pair.element0().versionKey();
    	  System.out.println("pair3-");
        long version = v.timestamp() + 1;
  	  System.out.println("pair4-");
        newData.addBasedOn(v);
    	  System.out.println("pair5-");
        //since we create a new version, we can access old versions as well
        return new Pair<Number160, Data>(new Number160(version,
            newData.hash()), newData);
      }
      
    } catch (NullPointerException e) {
      throw new UnhandleableInconsistencyException("have to handle this manually");
    }
  }
  
  private static <K> Pair<Number640, K> checkVersions(
      Map<PeerAddress, Map<Number640, K>> rawData) {
    Number640 latestKey = null;
    K latestData = null;
    for (Map.Entry<PeerAddress, Map<Number640, K>> entry : rawData
        .entrySet()) {
      if (latestData == null && latestKey == null) {
        latestData = entry.getValue().values().iterator().next();
        latestKey = entry.getValue().keySet().iterator().next();
      } else {
        if (!latestKey.equals(entry.getValue().keySet().iterator()
            .next())
            || !latestData.equals(entry.getValue().values()
                .iterator().next())) {
          return null;
        }
      }
    }
    return new Pair<Number640, K>(latestKey, latestData);
  }
  
/*************************************/
  
  public Conversation searchConversation(String senderId,String receiverId, PeerDHT peerDHT) throws ClassNotFoundException, IOException{
	    FutureGet fget = peerDHT.get(Number160.createHash(Conversation.createId(senderId, receiverId))).getLatest().start();
	    
	    fget.awaitUninterruptibly();
	    System.out.println("search conversation"+Conversation.createId(senderId, receiverId)+fget.isSuccess()+fget.data());
	    if (fget.isSuccess()) {
	      Data tmp = fget.data();
	      if (tmp != null) {
	        return (Conversation)tmp.object();
	      }
	    }
	    
	    return null;
  }
  
  public Conversation searchConversation(String senderId,String receiverId){
	    try {
	        return searchConversation(senderId,receiverId, getPeerDHT());
	      } catch (ClassNotFoundException | IOException e) {
	        return null;
	      }
	  
  }
  
  public Future<Boolean> storeConversation(Conversation _conversation)  {
	  	while (isWaitingChat()){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		  LOG.debug("storing conversation " + _conversation.getId());
		  
		  
	    FutureTask<Boolean> res = new FutureTask<Boolean>(new Callable<Boolean>() {
	      @Override 
	      public Boolean call() {
	    	  boolean success=false;
	        try {
	          LOG.debug("storing conversation inside futuretask" + _conversation.getId());
	          boolean res=false;
	          res = store(getPeerDHT(), _conversation, Number160.createHash(_conversation.getId()));
	          success=true;
	          for (int i=0;i<30 && (res) ==false;i++){
	        	  waitingChat=true;
	        	  Thread.sleep(2000);
	        	  res = store(getPeerDHT(), _conversation, Number160.createHash(_conversation.getId()));
	          }
	          waitingChat=false;
	          LOG.debug("store returned " + res + " for conversation " + _conversation.getId());
	          return res;
	        } catch (UnhandleableInconsistencyException e) {
	          LOG.debug("caught unhandleableinconsistencyexception for conversation " + _conversation.getId());
	          Conversation conv = searchConversation(_conversation.getSenderId(), _conversation.getReceiverId());
	          if (conv == null) {
	            // we're putting a new identity and chances are noone is going to want to take the same identity at the same time
	            try {
	            	
		              FuturePut future = peerDHT.put(Number160.createHash(_conversation.getId())).data(new Data(_conversation)).start();
		              future.awaitUninterruptibly();
		              if (checkVersions(future.rawResult()) == null){
		            	  System.out.println("retry2 -----------");
		               future = peerDHT.put(Number160.createHash(_conversation.getId())).data(new Data(_conversation)).start();
		              future.awaitUninterruptibly();
		              }else if (checkVersions(future.rawResult()) == null)
		            	  System.out.println("retry2-2 -----------");
		               future = peerDHT.put(Number160.createHash(_conversation.getId())).data(new Data(_conversation)).start();
		              future.awaitUninterruptibly();
	              for (int i=0;i<30 && future.isFailed();i++){
	            	  System.out.println("retry put -------------");
		              future = peerDHT.put(Number160.createHash(_conversation.getId())).data(new Data(_conversation)).start();
		              future.awaitUninterruptibly();
	            	  
	              }
	              LOG.debug("simple put successful for conversation " + _conversation.getId()+_conversation.getChat().values().toArray()[0]);
	              
	              return true;
	            } catch (IOException e1) {
	              e1.printStackTrace();
	            }
	            
	            LOG.debug("simple put failed for conversation " + _conversation.getId());
	          } else {
	            // TODO maybe handle
	            e.printStackTrace();
	          }         
	        } catch (ClassNotFoundException | InterruptedException | IOException e) {
	          e.printStackTrace();
	        }
	        
	        return false;
	      }
	    });
	    
	    Executors.newSingleThreadExecutor().execute(res);
	    return res;
	  }

  
  public void sendChatMessage(String senderId,String receiverId, String message)throws ClassNotFoundException, IOException{
	  /*
	    FutureGet fget = peerDHT.get(Number160.createHash(Conversation.createId(senderId, receiverId))).start();
	    Conversation conversation;
	    fget.awaitUninterruptibly();
	    if (fget.isFailed()){
	    	fget = peerDHT.get(Number160.createHash(Conversation.createId(senderId, receiverId))).start();
		    fget.awaitUninterruptibly();
	    }
	    if (fget.isFailed()){
	    	fget = peerDHT.get(Number160.createHash(Conversation.createId(senderId, receiverId))).start();
		    fget.awaitUninterruptibly();
	    }
	    */	
	    Conversation conversation = searchConversation(senderId,receiverId);
	    if (conversation ==null) {
	    	  conversation= new Conversation(senderId, receiverId);
	    }
	    conversation.addMessage(message);
	   // getPeerDHT().put(Number160.createHash(conversation.getId())).object(conversation).start();
	    System.out.println("conversation:"+conversation.getId()+conversation.getChat().size());
	    storeConversation(conversation);
	    
	  
  }
  public Message sendMessage(PeerAddress pa, Message m) throws ClassNotFoundException, IOException {
	  System.out.println("send message"+pa+m.getClass());
	FutureDirect fd=peer.sendDirect(pa).object(m).start();
	fd.awaitUninterruptibly();
	if (fd.isSuccess()){
		return (Message)fd.object();
	}
	return null;
  }

  public void sendVideoRequest(String id, VideoCallCallback cb) {
    Identity identity = searchIdentity(id);
    if (identity != null) {
      PeerAddress pa = identity.getLastPeerAddress();
      try {
        MessageVideo mv = (MessageVideo)sendMessage(pa, new MessageVideoRequest());
        if (mv != null) {
          cb.setPeerAddress(pa);
          cb.setAccept(mv.getAccept());
        } else {
          cb.setAccept(null);
        }
        
      } catch (ClassNotFoundException | IOException e) {
        e.printStackTrace();
      }
    }
  }

  public MessageLog sendLogfileRequest(String ip, int port) throws ClassNotFoundException, IOException {
    FuturePing fp = peer.ping().inetSocketAddress(new InetSocketAddress(Inet4Address.getByName(ip), port)).start();
    fp.awaitUninterruptibly();
    
    PeerAddress pa = fp.remotePeer();
      
    return (MessageLog) sendMessage(pa, new MessageLogRequest("p2pskype.log"));
  }

  private void startMessageListener() {
    peer.objectDataReply(new ObjectDataReply() {
      @Override
      public Object reply(PeerAddress sender, Object request) {
    	  System.out.println("incoming");
        Message incoming = (Message) request;
        if (incoming instanceof MessageVersionRequest) {
          MessageVersion outgoing = new MessageVersion();
          return outgoing;
        } else if(incoming instanceof MessageVideoRequest){
          if (incomingVideoCallCallback != null && videoTerminationCallback != null) {
            boolean res = incomingVideoCallCallback.call(sender);
            MessageVideo outgoing = new MessageVideo(res);

            return outgoing;
          }

        } else if (incoming instanceof MessageVideoTermination) {
          videoTerminationCallback.call(null);          
          
        } else if (incoming instanceof MessageLogRequest) {
          MessageLogRequest messageLogRequest = (MessageLogRequest) incoming;
          String logFileName = messageLogRequest.getLogFileName();
          MessageLog outgoing = new MessageLog(logFileName);
          return outgoing;
        }else  if (incoming instanceof OnlineStatusRequest) {
            OnlineStatusResponse outgoing = new OnlineStatusResponse();
            return outgoing;

        }else {
          messageList.add(incoming);
        }

        return null;
      }
    });
  }
  
  public void subscribeIncomingCall(Callback<PeerAddress, Boolean> cb) {
    this.incomingVideoCallCallback = cb;
  }
  
  public void subscribeVideoTermination(Callback<Void, Void> cb) {
    this.videoTerminationCallback = cb;
  }

  public List<challengetask.application.interfaces.Peer> getPeers() {
    Collection<PeerAddress> addressList = peer.peerBean().peerMap().all();
    
    List<challengetask.application.interfaces.Peer> neighbors = new LinkedList<>();
    
    // TODO: consider UDP ports also
    for (PeerAddress neighbor : addressList) {
      challengetask.application.interfaces.Peer peer = new challengetask.application.PeerImpl(neighbor.inetAddress().toString(), 
          Integer.toString(neighbor.tcpPort()), null);
      neighbors.add(peer);
      
      getVersion(neighbor, version -> peer.setVersion(version));
    }
    
    return neighbors;
  }
  
  public Future<Boolean> sendFriendRequest(Identity friend, Identity self) throws IOException {
    friend = searchIdentity(friend.getId());
    friend.getFriendRequests().add(self.getId());
    
    return putIdentity(friend);
  }

  public Future<Boolean> setOnlineStatus(Identity identity, boolean value) {
    identity = searchIdentity(identity.getId());
    identity.setOnlineStatus(value);
    try {
      return putIdentity(identity);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public boolean directOnlineStatusCheck(String friendId) throws ClassNotFoundException, IOException {
    Identity friendIdentity = searchIdentity(friendId);
    if (friendIdentity == null)
        friendIdentity = searchIdentity(friendId);
    if (friendIdentity == null)
        friendIdentity = searchIdentity(friendId);
   
    if (friendIdentity == null){
    	System.out.println(" not onine because identity not found"+friendId);
    	return false;
    }
    
    PeerAddress pa = friendIdentity.getLastPeerAddress();
    System.out.println("last peer address"+pa);
    OnlineStatusResponse response = (OnlineStatusResponse) sendMessage(pa, new OnlineStatusRequest());
    return response != null;
  }

  public void shutdown(){
	  LOG.debug("shutting down the peerDHT");
	  peerDHT.shutdown();
	  peer.shutdown();
  }
  public boolean isWaitingIdentity(){
	  return waitingIdentity;
  }
  public boolean isWaitingChat(){
	  return waitingChat;
  }
}