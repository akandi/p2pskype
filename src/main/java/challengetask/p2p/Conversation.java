package challengetask.p2p;

import java.io.Serializable;
import java.util.Date;
import java.util.SortedMap;
import java.util.TreeMap;

public class Conversation implements Serializable{

	private static final long serialVersionUID = 1133681048800798364L;
	
	private String senderId;
	private String receiverId;
	private SortedMap<Long, String> chat=new TreeMap<Long, String>();
	public Conversation(String sId,String rId){
		senderId=sId;
		receiverId=rId;
		
	}
	public String getId(){
		return createId(senderId,receiverId);
	}
	public void addMessage(String message){
		chat.put(new Date().getTime(), message);
	}
	public static String createId(String id1, String id2){
		return id1+"--->"+id2;
	}
	public SortedMap<Long, String> getChat(){
		return chat;
	}
	public String getSenderId(){
		return senderId;
	}
	public String getReceiverId(){
		return receiverId;
	}
	
}
