package challengetask.message;

public class MessageVideo extends Message {
	private static final long serialVersionUID = 1L;

	Boolean accept;

	public MessageVideo(Boolean accept){
		this.accept = accept;
	}
	public Boolean getAccept(){
		return accept;
	}
}
