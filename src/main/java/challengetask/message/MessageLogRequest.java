package challengetask.message;

public class MessageLogRequest extends Message {

	private static final long serialVersionUID = 1L;

	private String logFileName;

	public MessageLogRequest(String logFileName) {
		this.logFileName = logFileName;
	}

	public String getLogFileName() {
		return this.logFileName;
	}
}
