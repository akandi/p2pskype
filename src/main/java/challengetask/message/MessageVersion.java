package challengetask.message;

import challengetask.application.util.Constants;

public class MessageVersion extends Message {
	private static final long serialVersionUID = 1L;
	
	String version;
	
	public MessageVersion(){
		version = Constants.VERSION_NUMBER;
	}
	public String getVersion(){
		return version;
	}
}
