package challengetask.message;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MessageLog extends Message {

	private static final long serialVersionUID = 1L;

	private List<String> logEntries = new ArrayList<String>();

	public MessageLog(String logFileName) {
		try (BufferedReader br = new BufferedReader(new FileReader("logs/" + logFileName))) {
			String logEntry;
			while ((logEntry = br.readLine()) != null) {
				logEntries.add(logEntry);
			}
		}
		catch (Exception e) {
			System.err.println(e);
		}
	}
	
	public List<String> getEntries() {
	  return logEntries;
	}
}
