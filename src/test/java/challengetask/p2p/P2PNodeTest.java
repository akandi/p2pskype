package challengetask.p2p;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import challengetask.application.IdentityImpl;
import challengetask.application.interfaces.Identity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import challengetask.application.interfaces.Peer;
import challengetask.application.util.Constants;

public class P2PNodeTest {
  private P2PNode nodes[] = new P2PNode[21];
  private P2PNode node;
  private P2PNode otherNode;
  
  @Before
  public void doSetUp() {
    nodes[0] = new P2PNode();
    node = nodes[0];
    try {
      final int BOOTSTRAP_PORT = 1234;
      node.connect("localhost", BOOTSTRAP_PORT);
      
      for (int i = 0; i < 20; i++) {
        nodes[i + 1] = new P2PNode();
        otherNode = nodes[i + 1];
        otherNode.connect("localhost", BOOTSTRAP_PORT);
      }
      
      List<Peer> peers = node.getPeers();
      assertTrue(peers.size() > 1);
      peers.get(0).getVersion().equals(Constants.VERSION_NUMBER);
      
    } catch (Exception e) {
      assertTrue(false);
    }
  }
  
  @After
  public void shutdown() {
    for (P2PNode n : nodes) {
      n.shutdown();
    }
  }
  
  @Test
  public void testIdentities() {  
    Identity id = node.searchIdentity("asdf");
    assertNull(id);
    
    Identity putId = new IdentityImpl("asdf", "asdf");
    try {
      Future<Boolean> future = node.putIdentity(putId);
      try {
        assertTrue(future.get());
      } catch (InterruptedException | ExecutionException e) {
        assertTrue(false);
      }
      
      id = node.searchIdentity("asdf");
      assertNotNull(id);
      putId.getName().equals(id.getName());
      putId.getId().equals(id.getId());
      
      id = otherNode.searchIdentity("asdf");
      assertNotNull(id);
      putId.getName().equals(id.getName());
      putId.getId().equals(id.getId());
    } catch (IOException e) {
      assertTrue(false);
    }
  }
  
  @Test
  public void testFriendslist() {
    Identity thisId = new IdentityImpl("this", "this");
    Identity otherId = new IdentityImpl("other", "other");
    
    try {
      Future<Boolean> future = node.putIdentity(thisId);
      Future<Boolean> future2 = otherNode.putIdentity(otherId);
      assertTrue(future.get());
      assertTrue(future2.get());
      
      future = node.sendFriendRequest(otherId, thisId);
      assertTrue(future.get());
      
      otherId = otherNode.searchIdentity(otherId.getId());
      List<String> requests = otherId.getFriendRequests();
      
      assertTrue(requests.size() > 0);
      assertTrue(requests.get(0).equals("this"));
    } catch (Exception e) {
      assertTrue(false);
    }
  }
  
  @Test
  public void testFriendslistConcurrency() {
    Identity friend = new IdentityImpl("friend", "friend");
    Identity thisId = new IdentityImpl("this", "this");
    Identity otherId = new IdentityImpl("other", "other");
    
    try {
      Future<Boolean> future = node.putIdentity(friend);
      Future<Boolean> future2 = node.putIdentity(thisId);
      Future<Boolean> future3 = otherNode.putIdentity(otherId);
      assertTrue(future.get());
      assertTrue(future2.get());
      assertTrue(future3.get());
      
      // concurrent sending of friend requests
      future = node.sendFriendRequest(friend, thisId);
      future2 = otherNode.sendFriendRequest(friend, otherId);
      
      boolean res1 = future.get();
      boolean res2 = future2.get();
      
      friend = otherNode.searchIdentity(friend.getId());
      
      boolean success = ((res1 ^ res2) && friend.getFriendRequests().size() == 1) || ((res1 && res2) && friend.getFriendRequests().size() == 2);
      
      assertTrue(success);
    } catch (Exception e) {
      assertTrue(false);
    }
  }
  
  @Test
  public void testOnlineStatus() {
    Identity thisId = new IdentityImpl("this", "this");
    Identity otherId = new IdentityImpl("other", "other");
    thisId.setOnlineStatus(true);
    otherId.setOnlineStatus(true);
    
    try {
      Future<Boolean> future = node.putIdentity(thisId);
      Future<Boolean> future2 = otherNode.putIdentity(otherId);
      assertTrue(future.get());
      assertTrue(future2.get());
      
      future = node.setOnlineStatus(thisId, false);
      try {
        future.get();
      } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
      }
      
      node.shutdown();
      
      thisId.setOnlineStatus(true);
      
      Thread.sleep(500);
      
      thisId = otherNode.searchIdentity(thisId.getId());
      
      assertFalse(thisId.getOnlineStatus());
      
    } catch (Exception e) {
      assertTrue(false);
    }
  }
  
  /*@Test
  public void testConversations() {
    
  }
  
  @Test
  public void testDirectMessage() {
    
  }
  
  @Test
  public void testVideoRequest() {
    
  }
  
  @Test
  public void testVideoCallbacks() {
    
  }*/
}
